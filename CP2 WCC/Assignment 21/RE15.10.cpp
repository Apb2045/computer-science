//Review Exercise 15.10

#include <iostream>
using namespace std; 

class Base
  {
  public:
     virtual void display();
  };
  void Base::display()
  {
     cout << "In base class \n";
  }
  class Derived : public Base
  {
  public:
     Derived(int v );
     virtual void display();
  private:
int value; };
  Derived::Derived(int v)
  {
value = v; }

  void Derived::display()
  {
     cout << "In derived class, value is " << value << "\n";
  }
int main() {
     Base b;
     Derived d(4);
     b = d;
     b.display();
     Base* bp = new Derived(7);
     bp->display();
}

/* * * * * * * * * * * * * * * * * * * * * *

output: 

In base class 
In derived class, value is 7


 * * * * * * * * * * * * * * * * * * * * * */