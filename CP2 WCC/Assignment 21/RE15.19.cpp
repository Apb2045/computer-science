//Review Exercise 15.19

void f(const Fraction& a)
  {
     Fraction b = a;
     Fraction* c = new Fraction(3, 4);
     Fraction* d = &a;
     Fraction* e = new Fraction(7,8);
     Fraction* f = c;
     delete f;
}

/* * * * * * * * * * * * * * * * * * * * * *

b will be destroyed once it goes out of scope

the pointers c, d, e, f will be deleted

a will not be destroyed or deleted because it is a reference to an object outside of the function

 * * * * * * * * * * * * * * * * * * * * * */