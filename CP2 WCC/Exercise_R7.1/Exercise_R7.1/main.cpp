//
//  main.cpp
//  Exercise_R7.1
//
//  Created by Alexander P. Babich on 10/21/13.
//  Copyright (c) 2013 Alexander P. Babich. All rights reserved.
//

#include <iostream>
#include "ccc_time.h"
#include "ccc_empl.h"

using namespace std;

int main()
{
    int * p = new int; //creates a pointer that creates a new int !!!! need to make sure that there is a
    
    
    * p = 5; //the book says p = 5. This gives an error because p is an address not an integer *p is the integer at the location of p.
    
    
    (* p) = (* p) + 5; //didnt need to add the parantheses but its good practice

    
    Employee * e1 = new Employee ("Hacker, Harry",34000); //needed to make e1 a pointer because you cant assign a new object to a variable. Makes sense though but Why not???
    
    
    Employee * e2; //creates an employee object that is a variable
    
    e2->set_salary //this wont work because e2 is not a pointer either make e2 a pointer or use e2.set_salary()
    
    
    return 0;
}

