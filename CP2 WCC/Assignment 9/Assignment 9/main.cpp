/*****************************************************************************
 Name: Alex Babich
 Date: Thursday, October 10th 2013
 COMSC 110 Computer Programming II
 Assignment: Employee Vectors
 File Name: main.cpp
 Compiler Used: Xcode
 ****************************************************************************
 
 Problem Statement **********************************************************
 Put the problem statement in this section

 Create 2 classes with headers and implementation files.
 
 One for the pokemon class that has variables
 -name
 -type
 -level
 -strengh
 -attack
 
 with a default and a non default constructor
 
 
 One for the attack class that has variables
 -type
 -level
 -power points
 
 with a default and a non default constructor

 Create a driver program for the Monster Game that does the following;
 - Reads data for 3 PocketMonster objects
 - Stores the objects in a vector.
 - Displays all vector data
 
 Data Requirements **********************************************************
 var name                data type      description
 
 Input:
 
 type                    int            user inputs the type of pokemon
 level                   int            user inputs the level of the pokemon
 name                    string         user inputs the name of the pokemon
 strength                int            user inputs the strength of the pokemon
 attack_type             int            user inputs type of the attack
 attack_level            int            user inputs the level of the attack
 attack_power_points     int            user inputs the amount of power points of the attack
 
 Output:
 
 PokeVector              Pokemon         displays all of the user inputed data using get member functions
 
 
 Processing:
 
 attack                 Attack          stores the user inputed data from attack_type, attack_level, and attack_power_point in the object
 PokeVector             Pokemon         stores all of the data inputed from the user in a non default costructor or stores the default constructor
 
 
 Formulas: none
 
 
 Libraries:       iostream    string    vector    pokemon.h    attack.h
 
 Algorithm Design ***********************************************************
 
 1. Asks the user to input the type, level, name, strength, attack type, attack level, and attack power points for 2 pokemon
 
 2. Displays the 2 user inputed pokemon as well as the default pokemon (Unknown)
 
 3. Tells the user that the default pokemon is evolving
 
 4. Asks the user to input the type, level, name, strength, attack type, attack level, and attack power points for the new pokemon
 
 5. Displays the new team of pokemon
 
 Testing Specifications *****************************************************
 Put test cases in this section
 
 1. Made sure that the default constructor worked
 
 2. Tested the non default constructor as well as the set and get member functions
 
 3. Verifyed that the correct data was held in the array and displayed properly
 
 
 */
// End of documentation

// Compiler directives : libraries and namespaces **************************

#include <iostream>
#include <string>
#include "attack.h"
#include "pokemon.h"
#include <vector>

using namespace std;


// main function ***********************************************************

int main ()
{

int size;

cout << "enter size";
cin >> size;

vector <Pokemon*> pokeVector;

for (int i = 0; i < size; i++)
{
    pokeVector.pushback(new Pokemon());
    
    pokeVector->read();
}
    
for (int i = 0; i < size; i++)
{ 
    pokeVector[i]->display();
}
	// End of program statements
	cout << "Please press enter once or twice to continue...";
	cin.ignore().get();    			// hold console window open
	return EXIT_SUCCESS;           	// successful termination
    
}
// end main() **************************************************************


