
//
//  attack.cpp
//  Assignment 9
//
//  Created by Alexander P. Babich on 10/5/13.
//  Copyright (c) 2013 Alexander P. Babich. All rights reserved.
//

#include <iostream>
#include "attack.h"
#include <string>

using namespace std;

Attack::Attack()
{
    type = 4;
    
    level = 0;
    
    power_points = 30;
}


Attack::Attack(int attack_type , int attack_level , int attack_power_points)
{
    type = attack_type;
    
    level = attack_level;
    
    power_points = attack_power_points;
}

void Attack::set_type(int new_type) 
{
    type = new_type;
}

void Attack::set_level(int new_level)
{
    level = new_level;
}

void Attack::set_power_points(int new_power_points)
{
    level = new_power_points;
}

int Attack::get_attack_type() const
{
    return type;
}

int Attack::get_attack_level() const
{
    return level;
}

int Attack::get_power_points() const
{
    return power_points;
}





