//
//  pokemon.cpp
//  Assignment 9
//
//  Created by Alexander P. Babich on 10/5/13.
//  Copyright (c) 2013 Alexander P. Babich. All rights reserved.
//

#include "pokemon.h"


Pokemon::Pokemon()
{
    attack;
    
    name = "";
    
    type = 1;
    
    level = 25;
    
    strength = 25;
    
    status = true;
}

Pokemon::Pokemon(Attack pokemon_attack, string pokemon_name, int pokemon_type, int pokemon_level, int pokemon_strength, int attack_type , int attack_level , int attack_power_points)
{
    attack = new Attack(attack_type , attack_level , attack_power_points);
    
    name = pokemon_name;
    
    type = pokemon_type;
    
    level = pokemon_level;
    
    strength = pokemon_strength;
    
    status = true;
}

Pokemon::~Pokemon()
{
    if (attack != NULL)
    {
       delete attack;
       attack = NULL;
    }
}

void Pokemon::read()
{
        cout << "Choose your type of pokemon (1-fire , 2-water, 3-grass): ";
        cin >> type;
        cout << endl;
        cout << "Choose the name of your pokemon: ";
        cin >> name;
        cout << endl;
        cout << "Choose the level of your pokemon: ";
        cin >> level;
        cout << endl;
        cout << "Choose the strength of your pokemon ";
        cin >> strength;
        cout << endl;
        cout << "Choose the type of your pokemon's attack(1-fire , 2-water , 3-grass, 4-normal): ";
        cin >> attack_type;
        cout << endl;
        cout << "Choose the level of your pokemons attack: ";
        cin >> attack_level;
        cout << endl;
        cout << "Choose the power points of your pokemons attack ";
        cin >> power_points;
        cout << endl; 
        
        attack = new Attack(attack_type, attack_level, power_points);   
}

void Pokemon::display()
{
      cout << "Pokemon: " << i+1 << endl
           << "Name is: " << name << endl
           << "Type is (1-fire , 2-water, 3-grass, 4-normal): " << type << endl
           << "Level is: " << level << endl
           << "Strength is: " << strength << endl
           << "Attack type is (1-fire , 2-water , 3-grass, 4-normal):" << attack->attack_type << endl
           << "Attack level is: " << attack->attack_level << endl
           << "Attack power points is: " << attack->power_points << endl << endl;   
}
 
 
void Pokemon::set_attack(int attack_type , int attack_level , int attack_power_points)
{
    attack = Attack( attack_type , attack_level , attack_power_points);
}


void Pokemon::set_name(string new_name)
{
    name = new_name;
}


void Pokemon::set_type(int new_type)
{
    type = new_type;
}


void Pokemon::set_level(int new_level)
{
    level = new_level;
}


void Pokemon::set_strength(int new_strength)
{
    strength = new_strength;
}


void Pokemon::set_status(bool new_status)
{
     status = new_status;    
}

Attack Pokemon::get_attack() const
{
    return attack;
}


string Pokemon::get_name() const
{
    return name;
}


int Pokemon::get_type() const
{
    return type;
}


int Pokemon::get_level() const
{
    return level;
}


int Pokemon::get_strength() const
{
    return strength;
}

bool Pokemon::get_status() const
{
     return status;    
}

