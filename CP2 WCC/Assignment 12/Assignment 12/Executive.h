

#ifndef EXECUTIVE_H
#define EXECUTIVE_H

#include <iostream>
#include "Manager.h"

//Creates the class executive which inherets from the manager class
class Executive: public Manager


{
    public:
    
        //Constructor for the executive object. Inherets from the manager class
        Executive(string name, double salary, string dept);
    
        //Prints the data from the Executive object.
        virtual void print() const;
    
    private:
    
    
    
    
};


#endif
