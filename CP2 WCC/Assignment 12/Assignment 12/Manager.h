#ifndef MANAGER_H
#define MANAGER_H

#include "ccc_empl.h"


using namespace std;

/*
 Creates the class manager which inherets from the employee class
*/
class Manager: public Employee
{
    
    public:
    
        //Default constructor that inherets from the default employee constructor and then adds the department to the parameters
        Manager(string name, double salary, string dept);
    
    
        //Member function that is used to return the department from the default constructor
        string get_department() const;
    
        //Print function which displays the data from the manager object.
        virtual void print() const;
    
    private: //private data members for the member functions of the manager class
        string department;

};

#endif