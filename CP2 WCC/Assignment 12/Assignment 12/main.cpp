/*****************************************************************************
 Name: Alex Babich
 Date: Wednesday, October 23th 2013
 COMSC 110 Computer Programming II
 Assignment: Assignment 12
 File Name: main.cpp
 Compiler Used: Xcode
 ****************************************************************************
 
 Problem Statement **********************************************************
 Put the problem statement in this section
 
 
 
 PART I: PROGRAMMING EXERCISE
 
  Create the Manger class as a derived class of the Employee (textbook version) class.
  Create the Executive class as a derived class of the Manager class.
  See textbook for specifics of the Manager and Executive class.
  You will need the textbook Employee class source code files.
  Both the Manager and Executive classes will have a print function as described in the textbook.
  Do not make any changes to the Employee class source code files.
 
 
 PART II: USING THE CLASSES WITH POLYMORPHISM
 
 Write a C++ program which does the following:
 
  Create a vector of pointers to Employee objects.
  Read data for 1 Manager object and 1 Executive object.
  Store the objects in the vector.
  Display the data for the objects in the vector.
 
 
 Data Requirements **********************************************************
 var name                data type      description
 
 Input:
 
 workers                 Employee *     A vector that stores pointers to employee objects

 
 
 Libraries:       iostream    vector    Executive.h
 
 Algorithm Design ***********************************************************
 
 1. Creates a vector that stores two new employee objects
 
 2. Uses print member functions to display the data from the inhereted employee objects
 
 
 Testing Specifications *****************************************************
 Put test cases in this section
 
 1. Verified that the correct data was displayed and that the virtual functions worked properly for the proper class
 
 
 */
// End of documentation

// Compiler directives : libraries and namespaces **************************

#include <iostream>
#include <vector>
#include "Executive.h"

using namespace std;

//double operator + (Employee& a, double dollars); //This is the non member function way of overloading the operator

ostream& operator << (ostream& out, const Employee& m);

istream& operator >> (istream&, Employee& e);

int main()

{
    vector <Manager *> workers(2); //creates a vector of pointers to employee or inhereted objects.
    
    
    workers[0]= new Manager("Chris Bodley", 69000, "Electrical Engineering"); //creates 2 new employee objects during runtime
    workers[1]= new Executive("Freddie Garcia", 450000, "History");
    
    workers[0]->print(); //uses the print function to display the information from the employee objects
    cout << endl;
    workers[1]->print();

    if(workers[0] != NULL) //checks to make sure that the pointer is not NULL
    {
    delete workers[0]; //frees up the memory address that the pointer is pointing to.
    workers[0]= NULL; //gets rid of the loose pointer.
    }
    
    if(workers[1] != NULL)
    {
    delete workers[1];
    workers[1]= NULL;
    }
    
    Manager alex("Alex",50000,"IT");
    
    Employee jon;
    
    cout << "Enter name and salary" << endl;
    cin >> jon;
    cout << endl << jon << endl;
    
    
    
    
    
    alex += 100.00; //currently we have the += overloaded so thats what we are using now
    
    cout << alex.get_salary() << endl;
    
    
    
    
    return 0;
    
}

ostream& operator << (ostream& out, const Employee& m)
{
    out << m.get_salary() << " " << m.get_name();
    
    return out;
}

/*
double operator + (Employee& a, double dollars) //This is the non member function way of overloading the operator
{
    double money = a.get_salary() + dollars;
    a.set_salary(money);
    return a.get_salary();
    
}
*/

istream& operator >> (istream& in, Employee& e)
{
    string n;
    double s;
    
    in >> n >> s;
    
    e.set_name(n);
    
    e.set_salary(s);
    
    return in;
}