#include "Manager.h"
#include <iostream>


using namespace std;



    
Manager::Manager(string name, double salary, string dept)
   :Employee(name,salary)
{
    department = dept;
}
    
string Manager::get_department() const
{
    return department;
}

void Manager::print() const
{
    cout << "Employee Name is: " << get_name() << endl
         << "Employe Salary is: " << get_salary() << endl
         << "Is manager of: " << get_department() << endl;
}