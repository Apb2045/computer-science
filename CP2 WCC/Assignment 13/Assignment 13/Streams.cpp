#include <iostream>
#include <string>
#include <fstream>
#include <limits>

using namespace std;

int main()
{

ofstream outData; //create a data type that is an output of a file stream
string file; //the name of the file

cout << "enter name of file: ";

cin >> file; // gets the name of the file from the user
cout << endl;

outData.open(file.c_str()); //member finctions that access the data from a file as a string

string name;
int age;
string line;
string buffer;

for(int i = 0; i < 3 ; i++) // has the user enter a name and a age of a person
{
        cout << "enter name:";
        getline(cin,buffer);
        getline(cin,name);
        cout << endl << "enter age:";
        cin >> age;
        cout << endl;
    
        outData << name << " " << age << endl; //the name and age is put in to the file.
}

ifstream inData;
inData.open(file.c_str());
    

for(int j = 0; j < 3; j++)
{
    getline(inData, line);
    cout << line << endl;
}

return 0;
}
