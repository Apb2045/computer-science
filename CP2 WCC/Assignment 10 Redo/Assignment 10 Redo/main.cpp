#include <iostream>
#include <vector>
#include "ccc_time.h"
#include "ccc_empl.h"

using namespace std;

int main()
{
    int size = 5;
    vector <Employee*> e(size);
    
    Time ** t; //creates a pointer to a pointer time object.
    t = new Time * [size]; //now we use the pointer we are pointing at to make a 
    
    for (int i = 0; i < size; i++){
        e[i] = new Employee("Random Employee", 50000);
        t[i] = new Time();}
    
    for (int i = 0; i < size; i++){
        cout << i+1 << ". Employee Name: " << e[i]->get_name()
             << endl << i+1 << ". Employee Salary: " << e[i]->get_salary()
             << endl << i+1 << ". Time: " << t[i]->get_hours() << ":"
             << t[i]->get_minutes() << ":" << t[i]->get_seconds() <<endl << endl;}
    
    return 0;
}
