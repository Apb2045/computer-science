
****************************************************************************
Name: Alex Babich
Date: 9/12/13
COMSC 110 Computer Programming II
Assignment: Review Program (Assignment 2)
File Name: Random Array Math 
Compiler Used: CodeRunner
****************************************************************************

Problem Statement **********************************************************
     Put the problem statement in this section: 

	Write a program to randomly generate integer data values into an array which can hold 25 values. The 
	program should ask the user to enter the minimum and maximum values in a numeric range that is not 
	less than 1 and not greater than 500. Validate that the user enters the numbers correctly (minimum first >= 
	1 and then maximum <= 500) and correct the errors if not. The program should then generate 25 random 
	numbers in that range and store them in the array. After filling the array, the program should calculate the 
	total of all the values that are even numbers and the total of all the values that are odd numbers. Then the 
	program should calculate the square root of each of those totals (hint: cmath library function). Display all 
	25 values in the array. Display the total of the even numbers and the square root of that total. Display the 
	total of the odd numbers and the square root of that total.
	

Data Requirements **********************************************************
     Input Variables: min // minimum value of the random numbers 
					  max // maximum value of the random numbers
					
     Output Variables: anArray[] // the array that is outputted
						theOddTotal // the total of all of the odd values of the array
						theEvenTotal // the total of all of the even values of the array
						sqrtOdd // the square root of the total of all of the odd values from the array
						sqrtEven // the square root of the total of all of the even values from the array
						
     Processing Variables: SIZE // used to give the size to the array.
							low // accepts the min value and is used to give the lower boundary for random number generation
							high // accepts the max value and is used to give the upper boundary for random number generation
							oddTotal // the total of the odds which is passed to theOddTotal
							evenTotal // the total of the evens which is passed to theEvenTotal
							myArray // the origional array that is passed to functions
							anyArray // an array that is used in the calc totals function. myArray is passed to it.
							i, j, x // interger used for iteration through loops
							
     Relevant Formulas: ( low + rand( ) % (high - low + 1 ) ); // returns random numbers between high and low

						for (int x = 0; x < SIZE; x++)
							{
								if (anyArray[x] % 2 == 0) 
								{
									evenTotal = evenTotal + anyArray[x]; // calculates the even and odd totals
								} 
								
								else 
								{
									oddTotal = oddTotal + anyArray[x];
								}
								
							}
							
						sqrtEven = sqrt(theEvenTotal) // calculates the square root of the total of the even numbers
						

Algorithm Design ***********************************************************
     Put the algorithm solution in this section:

		-Get the lower and upper number from the user to form limits for the random numbers
		-Have a function use those limits to return random numbers 
		-Use a different function that puts the random numbers generated from the previous function into elements of an array
		-Have another functon that sorts through the array and adds up all of the odd and even numbers in to 2 separate groups
		-Use some variables to calculate the square root of the odd and even totals
		-Finally output the array and all of the totals that we have calculated

Testing Specifications *****************************************************
     Put test cases in this section:
         
enter a number for MINIMUM value between 1 and 500: 20


enter a number for MAXIMUM between your minimum value and 500: 78


The array is: [ 21 28 76 47 64 69 50 73 73 72 53 74 66 29 56 20 23 62 46 60 55 5
3 73 40 31 ]

The total of all of the even numbers is: 714

The total of all of the odd numbers is: 600

The square root of the total of the even numbers is: 26.7208

The square root of the total of the odd numbers is: 24.4949

Please press enter once or twice to continue...


// End of documentation

// Compiler directives : libraries and namespaces **************************

#include <iostream>
#include <string>
#include <cmath>
#include <ctime>

using namespace std;

// Function Declerations****************************************************

void minMax (int & min, int & max); 

int randNum (int low, int high);

void calcTotals (const int anyArray[], int & oddTotal, int & evenTotal, const int SIZE);

void displayArray (int anArray[], int theOddTotal, int theEvenTotal);

// Global Variables********************************************************




// main function ***********************************************************

int main ()
{		
	srand(time( 0 ) );
	
	int min, max,
	oddTotal = 0, 
	evenTotal = 0,
	SIZE = 25,
	myArray[SIZE];
	
	minMax (min, max);
	
	
	
	for (int i = 0; i < SIZE; i++)
	{
		myArray[i]= randNum (min, max);
	}
	
	calcTotals (myArray, oddTotal, evenTotal, SIZE);
	
	displayArray (myArray, oddTotal, evenTotal);
	

	// End of program statements
	cout << "Please press enter once or twice to continue...";
	cin.ignore().get();    			// hold console window open
	return EXIT_SUCCESS;           	// successful termination
    
}
// end main() **************************************************************


// Function that gives me my limits for random number generation************

void minMax (int & min, int & max)
{
	do
	{
		cout << "enter a number for MINIMUM value between 1 and 500: ";
		cin >> min;
		cout << '\n' << '\n';
	}	
	while(min < 1 || min > 500);
	
	do 
	{
		cout << "enter a number for MAXIMUM between your minimum value and 500: ";
		cin >> max;
		cout << '\n' << '\n';
	} 
	while (max < min || max > 500);
}

//**************************************************************************

// Function that generates random numbers***********************************

int randNum (int low, int high)
{
	return ( low + rand( ) % (high - low + 1 ) );
}

//**************************************************************************

// Function that calculates totals******************************************

void calcTotals (const int anyArray[], int & oddTotal, int & evenTotal, int SIZE)
{
	for (int x = 0; x < SIZE; x++)
	{
		if (anyArray[x] % 2 == 0) 
		{
			evenTotal = evenTotal + anyArray[x];
		} 
		
		else 
		{
			oddTotal = oddTotal + anyArray[x];
		}
		
	}
	
}

//***************************************************************************

// Function that prints out the array and all other information**************

void displayArray (int anArray[], int theOddTotal, int theEvenTotal)
{
	float sqrtEven = sqrt(theEvenTotal), 
		  sqrtOdd = sqrt(theOddTotal);
	
	cout << "The array is: [ ";
	
	for (int j = 0; j < 25; j++)
		{	
			
			cout << anArray[j] << " ";
		}
		
	cout << "]" << '\n'<< '\n';
	
	cout << "The total of all of the even numbers is: " << theEvenTotal << '\n' << '\n';
	
    cout << "The total of all of the odd numbers is: " << theOddTotal << '\n'<< '\n';

	cout << "The square root of the total of the even numbers is: " << sqrtEven << '\n'<< '\n';
	
	cout << "The square root of the total of the odd numbers is: "<<  sqrtOdd << '\n'<< '\n';	

}
