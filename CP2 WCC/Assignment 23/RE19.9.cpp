/* * * * * * * * * * * * * * * * * * * * * *
Review Exercise 19.9

What is virtual inheritance?

Virtual inheretance is a technique where a certain base class in a hirearchy is used to share its data members instances with any other usage of the same base class in a derived class 

What problem does this feature address?

The problem this addresses is the problem that arises due to multiple inheretence called the diamond problem. The diamond problem occors when a new class is implemented from two derived classes which both inheret from the base class

 * * * * * * * * * * * * * * * * * * * * * */