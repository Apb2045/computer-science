/* * * * * * * * * * * * * * * * * * * * * *
Review Exercise 19.3

What is a pure virtual member function?

A pure virtual member function is a member function which converts a normal class into an abstract class and it is implemented in a derived class only.
There is nothing written in the body of the class.


What is an abstract class?

An abstract class is one which is implimented to use as a base class and contains at least one pure virtual function.

 * * * * * * * * * * * * * * * * * * * * * */