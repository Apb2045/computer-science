/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 
Exercise R6.3. 
 
 Write C++ code for a loop that simultaneously computes both the maximum and    
 minimum of a vector.
 
 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <iostream>
#include <vector>

using namespace std;
int main(int argc, char *argv[]) 
{
	vector <int> v;
	int number;
	
	
	cout << "enter a number press 0 to exit" << endl;
	
	while( number != 0)
	{
		int i = 0;
		cin >> number;
		v.push_back(number);
		i++;
	}
	
	int smallest = v[0];
	int largest = v[0];
		
	for(int i = 0; i < v.size() - 1; i++)
	{	
		cout << v[i] << " ";
		if (smallest > v[i])
			smallest = v[i];
	}
	cout<< endl << "the lowest value is: "<< smallest << endl;
	
	for(int i = 0; i < v.size() - 1; i++)
	{	
		if (largest < v[i])
			largest = v[i];
	}
	cout << "the highest value is: "<< largest << endl;
	
	
cin.ignore().get();
return 0;
}