/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

Exercise R6.4. What is wrong with the following loop?

  vector<int> v(10);
  int i;

  for (i = 1; i <= 10; i++) v[i] = i * i;

Explain two ways of fixing the error.

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*

The loop will go to index 10 in the vector which is undefined since a vector of size 10 has its highest index at 9 

*/

/*

This is the first way of fixing it.

  vector<int> v(10);
  int i;

  for (i = 1; i < 10; i++) v[i] = i * i;

I have changed the less than or equal to to less than so that the highest index it goes to is 9
*/


/*

Here is another way of fixing it.

  vector<int> v(10);
  int i;

  for (i = 1; i <= 10; i++) v[i-1] = i * i;

making the index of the vector i-1 will allow i to go to 10 but will be the 9th index in the vector
*/