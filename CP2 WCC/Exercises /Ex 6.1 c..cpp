/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

Alex Babich
 
Exercise R6.1. 
 
Write code that fills a vector v with each set of values below
 
c.1 4 9 16 25 36 49 64 81 100
 
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <iostream>
#include <vector>

using namespace std;
int main()

 {
	
	vector<int> v;
	
	for(int i = 0; i < 10; i++)
	{
		v.push_back(i+1);
	}
	
	for(int i = 0; i < 10; i++)
	{
		 v[i]=(i+1)*(i+1);
		 cout << v[i] << endl;
	}
	
	cin.ignore().get();
	return 0;
}