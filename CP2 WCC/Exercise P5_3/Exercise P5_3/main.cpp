#include <iostream>
#include <string>
#include "Address.h"
#include <vector>

using namespace std;

int main()
{

    char Y_N;
    
    string buffer;
    
    int number = 0;
    string street = "";
    string city = "";
    string state = "";
    int postal_code = 0;
    string apartment = "";

    vector <Address> address_vector;
 
    for (int i = 0; i <2; i++)
    {
    do
    {
    cout << "Would you like to enter an address with apartment?(Y/N): ";
    cin >> Y_N;
    }
    while((Y_N != 'y') && (Y_N != 'Y') && (Y_N != 'n') && (Y_N != 'N') );
    
    
    if((Y_N == 'y') || (Y_N == 'Y'))
    {
        cout << "Enter House Number: ";
        cin >> number;
        cout << "Enter Street: ";
        getline(cin, buffer);
        getline(cin, street);
        cout << "Enter City: ";
        getline(cin, city);
        cout << "Enter State: ";
        getline(cin, state);
        cout << "Enter Postal Code: ";
        cin >> postal_code;
        cout << "Enter Apartment Number: ";
        getline(cin, buffer);
        getline(cin, apartment);
        
        
        address_vector.push_back(Address(number, street, city, state, postal_code, apartment));
    }
    
    else
    {
        cout << "Enter House Number: ";
        cin >> number;
        cout << "Enter Street: ";
        getline(cin, buffer);
        getline(cin, street);
        cout << "Enter City: ";
        getline(cin, city);
        cout << "Enter State: ";
        getline(cin, state);
        cout << "Enter Postal Code: ";
        cin >> postal_code;
        
        address_vector.push_back(Address(number, street, city, state, postal_code, apartment));
    }
        
    }

     
    address_vector[0].comes_before(address_vector[1]);
    return 0;
}
