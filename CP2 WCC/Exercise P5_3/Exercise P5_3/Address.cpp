#include "Address.h"
#include <string>
#include <iostream>

using namespace std;

Address::Address(int number_address, string street_address, string city_address, string state_address, int postal_code_address)
{
    number = number_address;
    
    street = street_address;
    
    city = city_address;
    
    state = state_address;
    
    postal_code = postal_code_address;
    
    apartment = " ";
    
}


Address::Address(int number_address, string street_address, string city_address, string state_address, int postal_code_address, string apartment_address)
{
    number = number_address;
    
    street = street_address;
    
    city = city_address;
    
    state = state_address;
    
    postal_code = postal_code_address;
    
    apartment = apartment_address;
    
}


void Address::set_number(int number_address)
{
    number = number_address;
}

void Address::set_street(string street_address)
{
    street = street_address;
}

void Address::set_city(string city_address)
{
    city = city_address;
}

void Address::set_state(string state_address)
{
    state = state_address;
}

void Address::set_postal_code(int postal_code_address)
{
    postal_code = postal_code_address;
}

void Address::set_apartment(string apartment_address)
{
    apartment = apartment_address;
}

int Address::get_number() const
{
    return number;
}

string Address::get_street() const
{
    return street;
}

string Address::get_city() const
{
    return city;
}

string Address::get_state() const
{
    return state;
}

int Address::get_postal_code() const
{
    return postal_code;
}

string Address::get_apartment() const
{
    return apartment;
}


void Address::print()
{
    cout << number << " " << street << " " << apartment << endl;
    
    cout << city << ", " << state << " " << postal_code << endl;
}


void Address::comes_before(Address ad2) //Yus!
{
    if (this->get_postal_code() > ad2.get_postal_code())
    {
        ad2.print();
        cout << endl;
        this->print();
        cout << endl;
    }
    else
    {
        this->print();
        cout << endl;
        ad2.print();
        cout << endl;
        
    }
    
    
}
