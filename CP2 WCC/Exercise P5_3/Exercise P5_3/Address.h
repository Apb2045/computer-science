#ifndef ADDRESS_H
#define ADDRESS_H
#include <string>

using namespace std;

class Address
{
    public:
    
    Address(int number_address, string street_address, string city_address, string state_address, int postal_code_address);
    

    Address(int number_address, string street_address, string city_address, string state_address, int postal_code_address, string apartment_address);
    
    void set_number(int number_address);
    
    void set_street(string street_address);
    
    void set_city(string city_address);
    
    void set_state(string state_address);
    
    void set_postal_code(int postal_code_address);
    
    void set_apartment(string apartment_address);
    
    
    int get_number() const;
    
    string get_street() const;
    
    string get_city() const;
    
    string get_state() const;
    
    int get_postal_code() const;
    
    string get_apartment() const;
    
    
    
    void print();//displays the data from the address, (street informaion on first line), (city, state, zip on second line )
    
    
    void comes_before(Address ad2);
    
    
    private:
    
    int number;
    string street;
    string city;
    string state;
    int postal_code;
    string apartment;

};

#endif