//  ****************************************************************************
//  Name: Alex Babich
//  Date: Tuesday, September 24th 2013
//  COMSC 110 Computer Programming II
//  Assignment: Person Class
//  File Name:person.h
//  Compiler Used: Xcode
//  ****************************************************************************

//  Compiler directives
#ifndef PERSON_H
#define PERSON_H

#include <string>

using namespace std;


// creates a class named person
class Person
{ 
    
public:
    

    //default constructor, constructs an object person
    Person(); 
    
    //constructs a person object with name and age from the main function
    Person(string person_name, int person_age);
    
    //sets the age of the person
    void set_age(int new_age);
    
    // sets the name of the person
    void set_name(string new_name);
    
    //Pre condition: gets the age of the person
    int get_age() const; 
    //Post condition: returns the person age
    
    
    //Pre condition: gets the name of the person
    string get_name() const; 
    //Post condition: returns the person name
    
private:
    
    string name;
    int age;
};


// end of the compiler directives
#endif