
//  ****************************************************************************
//  Name: Alex Babich
//  Date: Tuesday, September 24th 2013
//  COMSC 110 Computer Programming II
//  Assignment: Person Class
//  File Name:person.cpp
//  Compiler Used: Xcode
//  ****************************************************************************

//  Compiler directives ********************************************************

#include <string>
#include "person.h"

using namespace std;


//default construtor with default parameters
Person::Person()
{
    name = "Alex Babich";
    age = 22;
}

//non-default constructor
Person::Person(string person_name, int person_age)
{
    name = person_name;
    age = person_age;
}

//Member Funcions********************************

void Person::set_age(int new_age)
{
    age = new_age;
}

void Person::set_name(string new_name)
{
    name = new_name;
}

int Person::get_age() const
{
    return age;
}

string Person::get_name() const
{
    return name;

}

//************************************************
//vff


