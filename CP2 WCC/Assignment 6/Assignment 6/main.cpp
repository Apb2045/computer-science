/*
 ****************************************************************************
 Name: Alex Babich
 Date: Tuesday, September 24th 2013
 COMSC 110 Computer Programming II
 Assignment: Person Class
 File Name: main.cpp
 Compiler Used: Xcode
 ****************************************************************************
 
 Problem Statement **********************************************************
 Put the problem statement in this section
 
 Class Definition
 30 points
 
 ￼￼￼Write the class definition for a class named Person as follows:
 
 1. The class definition should be stored in a header file named person.h.
 2. There should be documentation for all class member functions.
 3. The class will have two private data fields: name and age.
 4. The class will have two constructors: One default constructor and one constructor with
    parameters.
 5. The class will have mutator (set) functions for the name and age data fields.
 6. The class will have accessor (get) functions for the name and age data fields.
 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 
 Class Member Function Implementation and Creating a Driver Program
 
 Write the implementation of the class member functions and a driver program as follows:
 
 1. The class member function implementations should be stored in the .cpp implementation file.
 2. There should be documentation for all function implementations.
    a. In the spirit of compiling as you go, it is a good idea to alternate writing a member function and testing it in your driver program. Then you would compile what you have so far, and, once you have no syntax errors, you can run your program. Again you will find it much easier to diagnose your errors when testing small pieces at a time.
 3. The driver program (main function) should be in its own .cpp file.
 4. The driver program should create two person objects as follows:
    a. Create a Person object using the default constructor.
    b. Read a name and age.
    c. Use the mutator set functions to set the name and age.
    d. Create a Person object using the constructor with parameters. Make up a name and age for
    this one in the code.
    e. Display the data fields for both Person objects using the accessor get functions.
 
 Data Requirements **********************************************************
 var name         data type      description
 
 Input:
 
 name             string          user inputed name of person
 age              int             user inputed age of person

 
 
 Output:
 
 defaultPerson    Person         displays default persons name and age
 people           Person         displays user inputed persons name and age
 
 
 Processing:
 
 defaultPerson    Person         default constructor has the default name and age from the Person class
 people           Person         holds the name and age of a person that is determined by the user
 buffer           string         holds the \n that is stored in memory so that the proper data can be accessed
 
 
 Formulas       none
 
 Libraries      iostream    string    person.h 
 
 Algorithm Design ***********************************************************
 
 1. Creates the name and age varialbes with initialized values
 
 2. Creates the defalut and not default constructor from the person class that will have a name and age
 
 3. Displays the default constructors default name and age using accessor member functions
 
 4. Asks the user to enter a name and an age
 
 5. Uses mutator member functions to assign the user inputed name and age to the people object
 
 6. Displays the name and age of the second person using accessor member funcions 
 
 
 Testing Specifications *****************************************************
 Put test cases in this section
 
 1. made sure that the default constructor was displayed properly
 
 2. checked that both accessor and mutator were both used properly in the program
 
 3. verifyed that the getline ojbect was used properly
 
 4. identifyed the correct user inputted information was displayed on the screen
 
 
 */
// End of documentation

// Compiler directives : libraries and namespaces **************************

#include <iostream>
#include <string>
#include "person.h"

using namespace std;


// main function ***********************************************************

int main ()
{
    
    //creates space in memory for data type string
    string name = "Default Name";
   
    //creates a space in memory for data type int
    int age = 50;
    
    //creates space in memory for data type string. Will only be used for holding '\n' character
    string buffer;
    
    //creates an object which is the default constructor from the person class
    Person defaultPerson;
    
    //creates a person object from the people class that stores name and age
    Person people(name, age);
    
    
    cout
    
    //displays the default age of the person using a member function
    << "Default Person age is: " << defaultPerson.get_age() << endl << endl
    
    //displays the default name of the person using a member function
    << "Default Person name is: " << defaultPerson.get_name() << endl << endl; 
    
    
    //asks the user for the age
    cout << "Enter age: ";
    cin >> age;
    cout << endl;
    
    //asks the user for the name
    cout << "Enter name: ";
    getline(cin,buffer);
    getline(cin,name);
    cout << endl;
    
    

    //sets the age of person to the one inputted by the user
    people.set_age(age);
    
    //sets the age of person to the one inputted by tue user
    people.set_name(name);
    
    cout
    
    //displays the age of the second person
    << "The age of second person is: " <<people.get_age() << endl << endl
    
    //displays the name of the second person
    << "The name of second person is: " <<people.get_name() << endl << endl;
    
    
    
    
    
    
    
	// End of program statements
	cout << "Please press enter once or twice to continue...";
	cin.ignore().get();    			// hold console window open
	return EXIT_SUCCESS;           	// successful termination
    
}
// end main() **************************************************************






