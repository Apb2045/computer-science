//
//  TravelClock.cpp
//  CH_8_1_Clock_Inheretence
//
//  Created by Alexander P. Babich on 10/21/13.
//  Copyright (c) 2013 Alexander P. Babich. All rights reserved.
//

#include "TravelClock.h"

TravelClock::TravelClock(bool mil, string loc, int diff) :Clock(mil)
{
    location = loc;
    
    time_difference = diff;
    
    while (time_difference < 0)
    {
        time_difference = time_difference + 24;
    }
    
    
}

string TravelClock::get_location() const
{
    return location;
}

int TravelClock::get_hours() const
{
    int h = Clock::get_hours();
    
    if(is_military())
    {
        return (h + time_difference) % 24;
    }
    
    else
    {
        h = (h+ time_difference) % 12;
        
        if (h == 0)
        {
            return 12;
        }
        
        else
        {
            return h;
        }
    }
}
/*
 A base class constructor is when you put the base class constructor in the constructor of a constructor
 that inherits the class from which we call the base class. Its important because the constructor can
 only get parameters which are private data members from the inhereted class.
 
*/