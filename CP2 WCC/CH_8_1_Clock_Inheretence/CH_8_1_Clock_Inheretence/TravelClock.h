#ifndef TRAVELCLOCK_H
#define TRAVELCLOCK_H

#include "Clock.h"


using namespace std;

class TravelClock : public Clock
{
    public:
    /**
        constructs a travel clock thst csn tell the time at a specified location.
        @param mil true if the clock uses military format 
        @param loc the location 
        @param diff the time differenc from the local time
     */
        TravelClock(bool mil, string loc, int diff); 
    
        virtual int get_hours() const;
    
        virtual string get_location() const;
    
    private:
    
        string location;
    
        int time_difference;
    
};

#endif