
#include <iostream>
#include <vector>
#include <iomanip>

#include "TravelClock.h"

using namespace std;


int main()
{
    vector <Clock * > clocks(3);
    
    clocks[0] = new Clock(true);
    clocks[1] = new TravelClock(true,"Rome",9);
    clocks[2] = new TravelClock(false,"Tokyo",-7);
    
    for (int i = 0; i < clocks.size(); i++)
    {
        cout << clocks[i]->get_location() << " time is "
             << clocks[i]->get_hours() << ":"
             << setw(2) << setfill('0')
             << clocks[i]->get_minutes() << endl;;
    }
    
    return 0;
}

/*
 
 CH 8.1 Notes

Make sure to make the base class public
 
class TravelClock : public Clock




*/