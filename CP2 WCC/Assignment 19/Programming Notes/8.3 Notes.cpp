//8.3 Overridong Member Functions

/*
The derived class can override functions from the base class.

if we are not happy with one of the member functions from the base class 

for instance if we want to change the get_hours() member function to take into account the time difference our psudo code might look something like this

int TravelClock:: get_hours() const
{
	int h = local hour value;
	if (clock uses military time)
	{
		return (h + time_difference) % 24
	}
	
	else
	{
		h = (h + time_difference % 12;
		if (h == 0) return 12;
		else return h;
	}
}

//Cool Thing!! 

if you invoke a member function on the implicit parameter, you don't specify the parameter but just write the member function name:

if(is_military())

The compiler interprets 

is_military()

as

implicit paremter. is_military();


if you are trying to define a new member function with the same name as a member function in the inhereted class you must
specify the scope of the member fucntion if not you will go in an infinite loop forever

for instance if we are trying to change the get_hours method then we might try to do something like this

int TravelClock::get_hours() const
{
	int h = get_hours();
	......
 
}

this will not work because the function will be calling itself forever

we must do this 

int TravelClock::get_hours() const
{
	int h = Clock::get_hours();
	....
}

that would then work for us.


We must use BaseClass::function notation to explicitly call a base-class function.


Common Error 8.2 Attempting to Access Private Base-Class Fields

if fileds in the base class are private then the inhereted class' member functions cannot access those private data members

double Manager::get_salary() const;
{
	return salary + bonus;
	//will return an error because "salary" is a private keyword in the base class
	
}

the way to make it work is 

double Manager::get_salary() const;
{
	return Employee::get_salary + bonus;
	//will return an error because "salary" is a private keyword in the base class
	
}

