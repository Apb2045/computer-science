//Syntax 8.2 Constructor with Base-Class initializer

DerivedClassName::DerivedClassName(expressions)
	: BaseClassName(Expressions)
{
	statements
}

//Example:

Manager::Magager(string name, double salary, sting dept)
	:Employee(name, salary)
{
	department = dept;
}

//Purpose:

//Supply the implementation of a constructor, initializing the base class before the body of the derived-class constructor.