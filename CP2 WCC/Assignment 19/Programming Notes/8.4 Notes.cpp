8.4 Polymorphism-means to have multiple shapes;

we could put all of the clock objects with the travelclock objects in a vector but then we will loose the information from the 
inhereted class

its therefore best to use a vector of pointers to the clock and travelclock objects
The reason why is that A derived-class pointer can be converted to a base-class pointer
This works because the pointer only points to the address of the first part of the object in memory which is always the clock

when getting the clocks from memory however we are faced with a problem. When dereferencing the objects only the clock member functions are called and not the overrided ones from the inhereted class. 

we can put the virtual keyword in the member functions that are common to both the base and inhereted class but do different things.

we must put the keyword in the base class but it is also good practice to put those keywords in the inhereted class as well

we only need to put the virtual keyword in the header file however. It is not necessary to put it anywhere else such as the implementation file.

the virtual keyword allows the function that is being called to be the correct one that is based on which class it is from.

since the complier figures this out at runtime we call it dynamic binding.

the traditional call is called static binding.

because the clocks vector collects a mixture of both knids of clock. Such a collection is called polymorphic. 

