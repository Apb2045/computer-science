#include <string>
#include <vector>
#include <iomanip>

using namespace std;

#include "clock.h"

class TravelClock : public Clock
{
public:
    /**
     Constructs a travel clock that can tell the time
     at a specified location.
     @param mil true if the clock uses military format
     @param loc the location
     @param diff the time difference from the local time
     */
    TravelClock(bool mil, string loc, int diff);
    virtual string get_location() const;
    virtual int get_hours() const;

    
private:
    string location;
    int time_difference;
    friend ostream& operator << (ostream& output, vector <Clock*> clocks)
    {
        for (int i = 0; i < clocks.size(); i++)
        {
            output << clocks[i]->get_location() << " time is "
            << clocks[i]->get_hours() << ":"
            << setw(2) << setfill('0')
            << clocks[i]->get_minutes()
            << setfill(' ') << "\n";
        }
        return output;
    }
};


