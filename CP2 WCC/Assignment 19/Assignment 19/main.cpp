#include <iostream>
#include <iomanip>
#include <vector>

using namespace std;

#include "travelclock.h"


int main()
{
    vector<Clock*> clocks(3);
    clocks[0] = new Clock(true);
    clocks[1] = new TravelClock(true, "Rome", 9);
    clocks[2] = new TravelClock(false, "Tokyo", -7);
    
    cout << clocks;
    
    return 0;
}