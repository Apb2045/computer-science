/*
****************************************************************************
Name:
COMSC 101 Computer Programming I
Assignment: 
File Name:
Compiler Used:
****************************************************************************

Problem Statement **********************************************************
    Put the problem statement in this section

Data Requirements **********************************************************
    Input Variables

    Output Variables

    Processing Variables

    Relevant Formulas

Algorithm Design ***********************************************************
    Put the algorithm solution in this section

Testing Specifications *****************************************************
    Put test cases in this section

*/
// End of opening documentation

//  Compiler directives ****************************************************
#include <iostream>

using namespace std;

//  Function Prototypes ****************************************************

//  main function **********************************************************
int main ()
{
	// All variable declarations go here at the top of the main function
	float x =5.6 , y =5 ;
	int z;

	// Executable statements following the algorithm go here
    
	z = x + y;

	// End of program statements
	cout << "Please press enter once or twice to continue...";
	cout << z;
	
	cin.ignore().get();    			// hold console window open
	return EXIT_SUCCESS;           	// successful termination
}
//  end main() *************************************************************

//  Function Implementations ***********************************************

