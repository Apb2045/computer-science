//
//  main.cpp
//  CP 2 Competition
//
//  Created by Alexander P. Babich on 11/13/13.
//  Copyright (c) 2013 Alexander P. Babich. All rights reserved.
//

#include <iostream>

/*
 
Ask the user to think of a number between 1 an 100000
 
have the computer ask if the number less than or greater than midpoint of int min = 1 or int max 1000000
 
if the user says that its less than make max become midpoint and leave min the same 
 
if the user says that its greater than make it so that the max stays the same and the min becoms midpoint
 
*/

using namespace std;

int guess(int min,int max);

int main()
{

    cout << "Think of a number between 1 and 1000000" << endl << endl;
    
    int min = 1, max = 1000000;
    
    cout << guess(min,max);
    

    return 0;
}

int guess(int min,int max)
{
    int midpoint = (min + max)/2;
    
    int num;

    cout << "Is your number greater than less than or equal to " << midpoint << "? (press 1 for greater than and 2 for less than and 3 for equal to)";
    cin >> num;
    
    
    if (num == 1)
    {
        min = midpoint;
        return guess(min,max);
    }
    
    else if (num == 2)
    {
        max = midpoint;
        return guess(min,max);
        
    }
    
    else
    {
        cout << "your number is" << midpoint << endl;
        return 0;
    }
}