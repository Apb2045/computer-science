/*
 ****************************************************************************
 Name:Alex Babich
 Date: September 19, 2013
 COMSC 110 Computer Programming II
 Assignment:USING CLASSES IN PROGRAMS
 File Name:main.cpp
 Compiler Used:Xcode
 ****************************************************************************
 
 Problem Statement **********************************************************
 Put the problem statement in this section
 
 Using the Time and Employee classes from the textbook, create a program that does the
 following.
 
 Do not modify the Time and Employee class files.
 *  Create a project.
 *  Copy the Time and Employee class files into the same folder as the project and add the
    files to the project.
 *  Create the source code file that contains the main function and create a program that does
    the following.
 a) Create 2 Time Objects.
    1 Time object should be instantiated using the default constructor.
 *  1 Time object should be instantiated using the constructor that requires
    hours, minutes and seconds. Read data from keyboard.
 b) Find the difference in hours, minutes and seconds between the two Time objects.
 c) Display all the data for both Time objects.
 d) Create an array to store 3 Employee objects. Using a for loop,
 *  Read name and salary for each Employee object
 *  Instantiate each Employee object using the constructor with parameters
    and assign the object to its position in the array.
 e) Using another for loop, raise the salary of all the objects in the array by 15%
 f) Display all the data for the Employee objects in the array
 
 
 Data Requirements **********************************************************
                var name      data type      description
 
 Input          hours         int            user inputed hours
                minutes       int            user inputed minutes
                seconds       int            user inputed seconds
                name          string         user inputs the employees name
                salary        double         user inputs the employees salary
            
 
 Output         whatTime      Time           displays the user inputted time variables
                timeNow       Time           displays time from the computer
                dhours        int            displays the difference in hours
                dminutes      int            displays the difference in minutes
                dseconds      int            displays the difference in seconds
                empArr[]      Employee       displays the data for all employees and their salaries

 Processing
                whatTime      Time           stores the user inputted time variables
                timeNow       Time           default constructor has the time from the computer
                dhours        int            holds the difference in hours
                dminutes      int            holds the difference in minutes
                dseconds      int            holds the difference in seconds
                empArr[]      Employee       holds the data for all employees and their salaries
                e1            Employee       holds the data for each individual employee which is then passed to empArr[]
                temp          double         holds the salary for each employee and then increses it by 15%
                
 
 Formulas       none
 
 Libraries      iostream    string    ccc_empl.h    ccc_time.h
 
 Algorithm Design ***********************************************************
 Put the algorithm solution in this section
 
 1. First runs the clock application
    a. gets hours , minutes, and seconds
    b. creates a variable that has the current time as well as a variable that takes in the time put in by the user
    c. gets the difference of the two times
    d. displays the current time, the entered time, and the difference of the times.
 
 2. Second runs the Employee application
    a.gets the employees names and salaries from the user
    b. stores it in an array
    c. takes the salaries from the employees and gives them all a 15% raise
    d. displays the names and salaries of the employees
 
 Testing Specifications *****************************************************
 
 1. Test valid and invalid minimum and maximum range values
 2. Test to make sure the correct time is displayed
 3. Test to make sure that the correct employees are in the array
 4. Test user interface funcitonality and appearance

*/

// End of documentation

// Compiler directives : libraries and namespaces **************************

#include <iostream>
#include <string>
#include "ccc_empl.h"
#include "ccc_time.h"

using namespace std;


// main function ***********************************************************

int main ()
{
    
    //Time display
    //**********************************************************************
    
    int hours, minutes, seconds;
    
    do
    {
        cout << "Enter hours: ";
        cin >> hours;
    }
    while (hours < 0 || hours > 23);
    
    do
    {
        cout << endl << "Enter minutes: ";
        cin >> minutes;
    }
    while (minutes < 0 || minutes > 59);
    
    do
    {
        
        cout << endl << "Enter seconds: ";
        cin >> seconds;
    }
    while (seconds < 0 || seconds > 59);
    
    
    cout << endl;
    
    Time timeNow;
    Time whatTime(hours,minutes,seconds);
    
  
    
    int dHours, dMinutes, dSeconds;
    
    dHours = abs(whatTime.get_hours()-timeNow.get_hours());
    dMinutes = abs(whatTime.get_minutes()-timeNow.get_minutes());
    dSeconds = abs(whatTime.get_seconds()-timeNow.get_seconds());
    
    cout << "Right now the time is: "<< timeNow.get_hours()
         << " Hours " << timeNow.get_minutes()
         << " Minutes " << timeNow.get_seconds()
         << " Seconds" << endl << endl;
    
    cout << "The time you entered is: "<< whatTime.get_hours()
         << " Hours " << whatTime.get_minutes()
         << " Minutes " << whatTime.get_seconds()
         << " Seconds" << endl << endl;
    
    cout << "The difference of the times is: "<< dHours
         << " Hours " << dMinutes
         << " Minutes " << dSeconds
         << " Seconds" << endl << endl
    
        <<"*****************************************************************************************" << endl << endl;
    
    //**********************************************************************
    
    //Employee Array
    //**********************************************************************
    
    Employee empArr[3];
    
    for(int i=0; i<3; i++)
    {
        string name;
        double salary;
        
        cout << "Enter employee # " << i + 1 << " name: ";
        cin >> name;
        cout << endl << endl << "Enter employee # " << i + 1 << " salary: ";
        cin >> salary;
        cout << endl << endl;
        
        Employee e1(name,salary);
        
        empArr[i] = e1;
        
        
    }
    
    for(int i=0; i<3; i++)
    {
        double temp;
        temp = empArr[i].get_salary();
        temp = temp*1.15;
        empArr[i].set_salary(temp);
    }
    
    cout << "Employee********************Salary" << endl 
         << empArr[0].get_name() <<"********************** $"<<  empArr[0].get_salary() << endl
         << empArr[1].get_name() <<"********************** $"<<  empArr[1].get_salary() << endl
         << empArr[2].get_name() <<"********************** $"<<  empArr[2].get_salary() << endl
         << endl << endl
    
         <<"*****************************************************************************************" << endl << endl;
    
    
	// End of program statements
	cout << "Please press enter once or twice to continue...";
	cin.ignore().get();    			// hold console window open
	return EXIT_SUCCESS;           	// successful termination
    
}
// end main() **************************************************************


