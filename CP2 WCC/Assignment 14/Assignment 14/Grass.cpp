//
//  File.cpp
//  Assignment 14
//
//  Created by Alexander P. Babich on 11/7/13.
//  Copyright (c) 2013 Alexander P. Babich. All rights reserved.
//

#include "Grass.h"


Grass::Grass() :Pokemon()
{
    chloraphil = 50;
}

Grass::Grass(string pokemon_name, int pokemon_level, int pokemon_strength, int attack_type , int attack_level , int attack_power_points, int pokemon_chloraphil):Pokemon(pokemon_name, pokemon_level, pokemon_strength, attack_type, attack_level,attack_power_points)
{
    chloraphil = pokemon_chloraphil;
}

void Grass::set_chloraphil(int temp)
{
    chloraphil = temp;
}

int Grass::get_chloraphil() const
{
    return chloraphil;
}

void Grass::read()
{
    //Pokemon::read();
    cout << "Choose the chloraphil of your Grass pokemon: ";
    cin >> chloraphil;
    cout << endl;
    
    
}

void Grass::display()
{
    //Pokemon::display();
    cout << "chloraphil is: " << chloraphil << endl;
    
}