//
//  attack.h
//  Assignment 11
//
//  Created by Alexander P. Babich on 10/5/13.
//  Copyright (c) 2013 Alexander P. Babich. All rights reserved.
//

#ifndef ATTACK_H
#define ATTACK_H


class Attack // creates a class attack
{
public:
    
    //default constructor gives the attack the default parameters
    Attack();
    
    //non-default constructor asks for the parameters from the user
    Attack( int attack_type , int attack_level , int attack_power_points );
    
    //gets the input to give the attack a type
    void set_type(int new_type);
    
    //gets the input to give the attack a level
    void set_level(int new_level);
    
    //gets the input to give the attack an amount of power points
    void set_power_points(int new_power_points);
    
    //displays the type of the attack (1-fire, 2-water, 3-grass, 4-normal)
    int get_attack_type() const;
    
    //displays the level necessary for the attack
    int get_attack_level() const;
    
    //displays the amount of power points of the attack
    int get_power_points() const;
    
private: //private data members to be used by member functions
    
    int type;
    
    int level;
    
    int power_points;
    
};

#endif