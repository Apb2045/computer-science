//
//  Fire.cpp
//  Assignment 14
//
//  Created by Alexander P. Babich on 11/7/13.
//  Copyright (c) 2013 Alexander P. Babich. All rights reserved.
//

#include "Fire.h"


Fire::Fire() :Pokemon()
{
    temperature = 50;
}

Fire::Fire(string pokemon_name, int pokemon_level, int pokemon_strength, int attack_type , int attack_level , int attack_power_points, int pokemon_temperature):Pokemon(pokemon_name, pokemon_level, pokemon_strength, attack_type, attack_level,attack_power_points)
{
    temperature = pokemon_temperature;
}

void Fire::set_temperature(int temp)
{
    temperature = temp;
}

int Fire::get_temperature() const
{
    return temperature;
}

void Fire::read()
{
    //Pokemon::read();
    cout << "Choose the temperature of your fire pokemon: ";
    cin >> temperature;
    cout << endl;
    
    
}

void Fire::display()
{
    //Pokemon::display();
    cout << "Temperature is: " << temperature << endl;
    
}