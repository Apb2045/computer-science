/*****************************************************************************
 Name: Alex Babich
 Date: Thursday, November 21st 2013
 COMSC 110 Computer Programming II
 Assignment: Designing Classes IV
 File Name: main.cpp
 Compiler Used: Xcode
 ****************************************************************************
 
 Problem Statement **********************************************************
 Put the problem statement in this section
 
 DESIGNING AND MODIFYING CLASSES FOR THE POCKET MONSTER GAME
 
 PocketMonster Class
 Modify the PocketMonster class as follows:
  Change the Weapon object data member so that it is a pointer to a Weapon object.
  Modify all member functions that work with the Weapon object so that they are working with a pointer to a
 Weapon object.
  Add a bool property to be used to indicate player status. It will be used to determine if a player is still active or
 dead.
  Create a destructor function for object pointer cleanup.
  Create a read function to read all the data for a PocketMonster object – including the Weapon object
  Create a display function to display all the data for a PocketMonster object – including the Weapon object
  Create appropriate validation functions if not already done. Decide where they should go
 – in the class, as non-member functions, or in a separate library file for generic functions
 The type property should be removed from the PocketMonster class. The methods that manipulate the type
 property should be removed. This includes any references to the type property in the constructors, accessor,
 mutator, read and display functions.
  The playerName property stays in the PocketMonster class.
  The level property stays in the PocketMonster class.
  The strength property stays in the PocketMonster class.
  The status property stays in the PocketMonster class.
  The *weaponOne pointer property stays in the PocketMonster class
 
  Create two derived classes from the PocketMonster class (choose 2 of the three types of monsters used in
 previous assignments - Fire, Water and Grass)
 o Add a least one new property to each derived class that is specific to that class. For example, Fire objects
 may have a property that the other classes wouldn’t have, like temperature, or Water objects may have a
 pressure property.
 o Add a least one method that is specific to each derived class. For example, Fire objects need a method to
 turn temperature up and Water objects need a method to change a pressure property.
 o Include appropriate constructors, destructors, mutator, accessor, read and display functions in each
 derived class. Choose default value that the added property should start at.
 o Remember that derived class constructors need to use base class initialization to initialize the base class
 object.
 
 
 MONSTER GAME PROGRAM SIMULATION
 
 Create a C++ program which does the following -- NOT A DRIVER, A COMPLETE PROGRAM. Also note that there
 are additional functions that need to be created.
 
  Use a polymorphic vector or dynamic array to store pointers to PocketMonster objects. This structure will
 contain instances of both of the derived classes.
  Display a description of the game – use a function to do this.
  Using a function, ask the user how many PocketMonsters they want to play the game and validate that they
 enter 0-10. If they enter 0, the game should end. If they enter an invalid number, they should have to enter it
 again.
  Tell the user to enter the data for all of the derived class objects, including the base class data required for each
 derived class object.
  Use repetition and a random number generation function to simulate game playing. Run this routine at least 20
 times. Recommendation for random number range is -20 to +20.
 o Modify each player’s level and strength values by increasing them or decreasing them randomly
 (generate random negative and positive numbers).
 o Modify each player’s weapon durability values by increasing them or decreasing them randomly
 (generate random negative and positive numbers).
 o Each time a player has been modified, test the levels and implement the following conditions.
  If a player level or strength <= 0, the player is out of the game and is a loser—status changes.
  If a player’s weapon durability value <= 0, the player is out of the game and is a loser—status
 changes.
  If a player level >= 200, the player wins the game and the game is over.
  If there are no winners, the player with the highest player positive level is the winner, all others are losers. There
 are no winners if all players have a negative level value.
  Display the data for the winner if there is one.
  Display the data for all the losers.
 
 UPDATE FOR ASSIGNMENT 4
 
  Create an overloaded >> operator function to behave the same as the read function.
  Create an overloaded << operator function to behave the same as the display function.
  Create an overloaded += operator function which can be used when randomly modifying levels
 during game play.
  Create overloaded <, > and = = operator functions which can be used to compare 2 players
 of any of the derived class types. These should only compare the level, strength and status data
 members that are in the base class.
  Create an overloaded = operator function which can assign a derived class object to another derived
 class object. These should only assign the level, strength and status data members that are in the
 base class.
 
 Data Requirements **********************************************************
 var name                data type      description
 
 Input:
 
 size                    int            user inputs the size which is used by the getSize to determine the number of players

 
 Output:
 
 j                       int            used to display the numbers of the losing pokemon
 
 
 Processing:
 
 pokeVector              vector         stores the data for all of the information and is passed to the battleFunction() and displayFunction()
 players                 int            the amount of players that will be in the round
 size                    int            returns to the players
 i                       int            used to iterate through loops
 highest                 int            used to compare with the level of pokemon
 locale                  int            used to determine position of winning pokemon
 temp                    int            used to determine if the pokemon is a fire or a grass type
 
 Formulas: none
 
 
 Libraries:       iostream    cstdlib    vector    pokemon.h    attack.h     Fire.h     Grass.h     ctime
 
 Algorithm Design ***********************************************************
 
 1. Gets the Amount of pokemon the user wantes to play with.
 
 2. Has the user input the information for the pokemon they will be using.
 
 3. Puts the pokemon through a battle so that their level, strength, and attack is added to and subtracted by random numbers.
 
 4. Displays the winner based on who has the highest level
 
 5. Displays the rest of the team as losers
 
 Testing Specifications *****************************************************
 Put test cases in this section
 
 1. Made sure that user could only choose a team of 1 to 10 pokemon
 
 2. Tested that there could only be one winner but there doesnt always have to be
 
 3. Verifyed that the correct data was held in the array and displayed properly
 
 
 */
// End of documentation

// Compiler directives : libraries and namespaces **************************
#include <cstdlib>
#include <iostream>
#include "attack.h"
#include "pokemon.h"
#include "Fire.h"
#include "Grass.h"
#include <vector>
#include <ctime>

using namespace std;


void description();

int getSize(); //gets the amount of pokemon that the user would like to play with

int randNum (); //random number that is used for battle

void battleFunction(vector<Pokemon *>, int size); //used to have the pokemon fight eachother

void displayFunction(vector<Pokemon *>, int size); //used to display the current status of the pokemon

istream & operator >> (istream & in, Pokemon * p); //used to read the data of the pokemon objects

ostream & operator << (ostream & out, Pokemon * p); //used to display the date of the pokemon objects

bool operator == (const Pokemon& p1, const Pokemon& p2); //used to check if pokemon have the same status

bool operator < (const Pokemon& p1, const Pokemon& p2); //used to check if one pokemon is stronger than the other

bool operator > (const Pokemon& p1, const Pokemon& p2); // used to chack if one pokemon has a higher level than the other


int main()
{

    description();
    
    srand(time( 0 ) ); //creates a random number that uses the clock from the computer

    int players = getSize(); //puts the return from the value of the function get size to the players int
    
    
    vector<Pokemon *> pokeVector(players); //creates a vector of pointers to pokemon objects
    
    int temp = 0;
    
    for(int i = 0; i < players; i++) //loop that has the user enter the data for each of the pokemon that they will be playing with
    {
        
        do
        {
        cout << "Enter 1 for fire 2 for grass: ";
        
        cin >> temp;
        
        cout << endl;
        }
        while((temp < 1) || (temp > 2));
        
        switch (temp) { //switch statement that assigns the inhereted class based on the choice from the user.
            case 1:
                pokeVector[i] = new Fire();
                cin >> pokeVector[i];
                pokeVector[i]->read();
                break;
            case 2:
                pokeVector[i] = new Grass();
                cin >> pokeVector[i];
                pokeVector[i]->read();
            default:
                break;
        }
         
        

    }
    
    if((*pokeVector[0]) == (*pokeVector[1])) cout << "First and Second Pokemon have the Same Status" << endl << endl; //compares satus of 2 pokemon
    else cout << "First and Second Pokemon do not have the same Status" << endl << endl;
    
    if((*pokeVector[0]) < (*pokeVector[1])) cout << "Second Pokemon is stronger then first pokemon" << endl << endl;
    else cout << "Second Pokemon is not stronger then first pokemon" << endl << endl; // compares strength
    
    if((*pokeVector[0]) > (*pokeVector[1])) cout << "First Pokemon has a higher level then Second Pokemon" << endl << endl;
    else cout << "First Pokemon does not have a higher level then Second Pokemon" << endl << endl; //compares level
    
    
    battleFunction(pokeVector,players); //passes the vector with all the pokemon pointers and the size of that vector to the playFunction
    
    displayFunction(pokeVector, players); //passes the vector with all the pokemon pointers and the size of that vector to the playFunction

    return 0;
}

istream & operator >> (istream & in, Pokemon * p) //used to have the players input the values to the pokemon
{
    int level;
    string name;
    int strength;
    int attackType;
    int attackLevel;
    int attackPowerPoints;
  
    cout << "Choose the name of your pokemon: ";
    in >> name;
    cout << endl;
    cout << "Choose the level of your pokemon: ";
    in >> level;
    cout << endl;
    cout << "Choose the strength of your pokemon: ";
    cin >> strength;
    cout << endl;
    cout << "Choose the type of your pokemon's attack(1-fire , 2-water , 3-grass, 4-normal): ";
    cin >> attackType;
    cout << endl;
    cout << "Choose the level of your pokemons attack: ";
    cin >> attackLevel;
    cout << endl;
    cout << "Choose the power points of your pokemons attack: ";
    cin >> attackPowerPoints;
    cout << endl;
    p->set_name(name);
    p->set_level(level);
    p->set_strength(strength);
    p->set_attack(attackType, attackLevel, attackPowerPoints);
    
    return in;
}

ostream & operator << (ostream & out, Pokemon * p) //used to have the information displayed to the screen
{

    
    cout << "Name is: " << p->get_name() << endl
    << "Level is: " << p->get_level() << endl
    << "Strength is: " << p->get_strength() << endl
    << "Attack type is (1-fire , 2-water , 3-grass, 4-normal):" << p->get_attack().get_attack_type() << endl
    << "Attack level is: " << p->get_attack().get_attack_level() << endl
    << "Attack power points is: " << p->get_attack().get_power_points() << endl;
    return out;
}

bool operator == (const Pokemon& p1, const Pokemon& p2) //compares the status
{

    if(p1.get_status() == p2.get_status()) return true;
    else return false;
}

bool operator < (const Pokemon& p1, const Pokemon& p2) //compares the strength
{
    if(p1.get_strength() < p2.get_strength()) return true;
    else return false;
}

bool operator > (const Pokemon& p1, const Pokemon& p2) //compares the level
{
    
    if(p1.get_level() > p2.get_level()) return true;
    else return false;
}


void description()
{
    cout << "Hello there! Welcome to the world of Pokemon! This world is inhabited by creatures called Pokemon! For some people, Pokemon are pets. Other use them for fights. Today we will be battling some wild Pokemon. Choose and design up to 10 Pokemon and have them battle it out to see who comes out on top!" << endl << endl;
}


int getSize() //function that gets the size of the users party of pokemon and returns it making sure that it is within the allowed boundaries
{
    int size = 0;
    
    do
    
    {
        
        cout << "Please enter size: ";
    
        cin >> size;
    
        cout << endl;
        
    }
    
    while (size < 2 || size > 10);
    
    
    
    return size;
}


int randNum ()
{
	return ( -20 + rand( ) % (41) ); //creates random numbers between -20 and 20
}

void battleFunction(vector <Pokemon *> pokeVector, int size) //function that randomly sets values to level strength and attack to the pokemon
{
    
    for(int i = 0; i <= 20; i++)
    {
        for(int i = 0; i < size; i++)
        {
            (*pokeVector[i]) += randNum(); //->set_level(pokeVector[i]->get_level() + randNum()); //+= randNum();
        
            pokeVector[i]->set_strength(pokeVector[i]->get_strength() + randNum());
        
            pokeVector[i]->get_attack().set_power_points(pokeVector[i]->get_attack().get_power_points() + randNum());
        
            if ((pokeVector[i]->get_level() <= 0 ) || (pokeVector[i]->get_strength() <= 0)) //if the level or strength becomes less than zero the
                                                                                            //the pokemon faints
            {
                pokeVector[i]->set_status(false);
            }
        
            else if(pokeVector[i]->get_attack().get_power_points() <= 0) //if powerpoints of the pokemons attack becomes less than zero he faints
            {
                pokeVector[i]->set_status(false);
            }
            if (pokeVector[i]->get_level() >= 200 ) //if the level ever becomes greater than 199 the pokemon is the winner and the loop exits
            {
                return;
            }
        }
    }
}

void displayFunction(vector<Pokemon *> pokeVector, int size) //function that takes in the vector of pointers to pokemon objects and the size of it
{
    int highest = 0; //creates a varable highest that starts at 0
    int locale = -1; //creates a variable that corresponds to the index of the winning pokemon, its kept at -1 in case there isn't one
    int j = 0; //used to display numbers of the losing pokemon
    
        for(int i = 0; i < size; i++)
    {
        if(pokeVector[i]->get_level() >= 200) //if there is a pokemon with level 200 or above then they will be displayed as the winner
        {
            cout << "The Winner is: " << endl << endl;
            cout << pokeVector[i];
            pokeVector[i]-> display();
            cout << endl << endl;
            locale = i; //makes sure to make the locale the value of the winner so he is not diplayed as a loser as well (Refer to line 147)
        }
        else
        {
            for(int i = 0; i < size; i++) //if there hasn't been a winner then there will be this second test to determine a winner
            {
                if(pokeVector[i]->get_level() > highest) //tests if the pokemons level is higher then highest. If so then replaces that value with the new highest and continues
                                                         //until it gets to the end of the loop.
                {
                    highest = pokeVector[i]->get_level(); // sets the higest to the pokemon with the highest level
                    locale = i;  //sets the value of local to the index of the pokemon with the highest level
                }
            }
            if(locale >= 0)  //if there is a winner then the locale will be greater than 0 because if not then locale will never change from -1
            {
            cout << "The Winner is: " << endl << endl;
            cout << pokeVector[locale];
            pokeVector[locale]-> display(); //displays the winner as the pokemon in the position of locale
            cout << endl << endl;
            break;
            }
        }
    }
    
    if((size) > 2) cout << "The Losers are: " << endl << endl; //displays all of the pokemon that are not in the position locale
    else cout << "The Loser is: " << endl << endl;
    for(int i = 0; i < size; i++)
    {
        if(locale != i)
        {
            j++;
            cout << "Loser: "<< j << endl;
            cout << pokeVector[i];
            pokeVector[i]-> display();
            cout << endl << endl;
        }
    }
    cout << "Loser: "<< j << " Takes the level, strength and status of the winning pokemon" << endl << endl;
    (*pokeVector[j]) = (*pokeVector[locale]);
    cout << "The new data of loser " << j << "is: " << endl << endl << pokeVector[j];
}





















