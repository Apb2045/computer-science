//
//  pokemon.cpp
//  Assignment 11
//
//  Created by Alexander P. Babich on 10/5/13.
//  Copyright (c) 2013 Alexander P. Babich. All rights reserved.
//

#include "pokemon.h"


Pokemon::Pokemon()
{
    
    name = "";
    
    level = 25;
    
    strength = 25;
    
    status = true;
    
    attack = new Attack();
    
}

Pokemon::Pokemon(string pokemon_name, int pokemon_level, int pokemon_strength, int attack_type , int attack_level , int attack_power_points)
{
    
    name = pokemon_name;
    
    level = pokemon_level;
    
    strength = pokemon_strength;
    
    status = true;
    
    attackType = attack_type;
    
    attackLevel = attack_level;
    
    attackPowerPoints = attack_power_points;
    
    attack = new Attack(attackType , attackLevel , attackPowerPoints);
    
}

Pokemon::~Pokemon()
{
    if (attack != NULL)
    {
        delete attack;
        attack = NULL;
    }
    
}

void Pokemon::read()
{

    cout << "Choose the name of your pokemon: ";
    cin >> name;
    cout << endl;
    cout << "Choose the level of your pokemon: ";
    cin >> level;
    cout << endl;
    cout << "Choose the strength of your pokemon: ";
    cin >> strength;
    cout << endl;
    cout << "Choose the type of your pokemon's attack(1-fire , 2-water , 3-grass, 4-normal): ";
    cin >> attackType;
    cout << endl;
    cout << "Choose the level of your pokemons attack: ";
    cin >> attackLevel;
    cout << endl;
    cout << "Choose the power points of your pokemons attack: ";
    cin >> attackPowerPoints;
    cout << endl;
    
    attack = new Attack(attackType , attackLevel , attackPowerPoints);
}

void Pokemon::display()
{
    cout << "Name is: " << name << endl
         << "Level is: " << level << endl
         << "Strength is: " << strength << endl
         << "Attack type is (1-fire , 2-water , 3-grass, 4-normal):" << attack->get_attack_type() << endl
         << "Attack level is: " << attack->get_attack_level() << endl
         << "Attack power points is: " << attack->get_power_points() << endl;
}


int Pokemon::operator += (const int lev)
{
    return level += lev;  

}

void Pokemon::operator = (const Pokemon& p)
{
    set_level(p.get_level());
    set_status(p.get_status());
    set_strength(p.get_strength());
}

void Pokemon::set_attack(int attack_type , int attack_level , int attack_power_points)
{
    attack = new Attack( attack_type , attack_level , attack_power_points);
}


void Pokemon::set_name(string new_name)
{
    name = new_name;
}


void Pokemon::set_level(int new_level)
{
    level = new_level;
}


void Pokemon::set_strength(int new_strength)
{
    strength = new_strength;
}


void Pokemon::set_status(bool new_status)
{
    status = new_status;
}

Attack Pokemon::get_attack() const
{
    return * attack;
}


string Pokemon::get_name() const
{
    return name;
}


int Pokemon::get_level() const
{
    return level;
}


int Pokemon::get_strength() const
{
    return strength;
}

bool Pokemon::get_status() const
{
    return status;
}
