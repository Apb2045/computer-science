//
//  pokemon.h
//  Assignment 11
//
//  Created by Alexander P. Babich on 10/5/13.
//  Copyright (c) 2013 Alexander P. Babich. All rights reserved.
//

#ifndef POKEMON_H
#define POKEMON_H

#include <iostream>
#include "attack.h"
#include <string.h>

using namespace std;

class Pokemon //creates a class pokemon
{
    
public:
    
    Pokemon(); //default constructor that holds default values for all standard objects as well as the default constructor for the attack object
    
    
    Pokemon(string pokemon_name, int pokemon_level, int strength, int attack_type, int attack_level, int attack_power_points); //non default constructor requires
                              //the user to input all values
    
    ~Pokemon(); //creates a default deconstructor
    
    
    int operator += (const int lev); //operator overload += to be able to add to the level to get a new level
    
    void operator = (const Pokemon& p);
    
    virtual void read(); //reads the data of the pokemon from the user
    
    
    virtual void display(); //displays the data of the pokemon
    
    
    void set_attack(int attack_type , int attack_level , int attack_power_points); //sets the attack to a new attack with the user inputed
                                                                                   //parameters
    
    void set_name(string new_name); //sets user inputed name of the pokemon
    
    
    void set_level(int new_level); //sets the user inputed level of the pokemon
    
    
    void set_strength(int new_strength); //sets the user inputed strength of the pokemon
    
    
    void set_status(bool new_status); //sets the status of the pokemon. If it becomes false then he is out of the game
    
    
    Attack get_attack() const; //gets the attack obeject of the pokemon, attack member functions can be used to access the data
    
    
    string get_name() const; //gets the name of the pokemon to display

    
    int get_level() const; //gest the level of the pokemon to display
    
    
    int get_strength() const; // gets the strength of the pokemon to display
    
    
    bool get_status() const; //gets the status of the pokemon
    
    
    
    
private: //private data members to be used by member functions of the pokemon class only
    
    Attack * attack; //points to the address of an attack object
    
    string name;
    
    int level;
    
    int strength;
    
    bool status;
    
    int attackType;
    
    int attackLevel;
    
    int attackPowerPoints;
    
};

#endif