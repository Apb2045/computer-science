//
//  Fire.h
//  Assignment 14
//
//  Created by Alexander P. Babich on 11/7/13.
//  Copyright (c) 2013 Alexander P. Babich. All rights reserved.
//

#ifndef FIRE_H
#define FIRE_H

#include <iostream>
#include "pokemon.h"

class Fire: public Pokemon
{
    public:
    
        Fire();
    
        Fire(string pokemon_name, int pokemon_level, int pokemon_strength, int attack_type , int attack_level , int attack_power_points, int pokemon_temperature);
    
        void set_temperature(int temp);
    
        int get_temperature() const;
    
        virtual void read();
    
        virtual void display();
    
    private:
    
        int temperature;
    
};

#endif
