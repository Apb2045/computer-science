//
//  File.h
//  Assignment 14
//
//  Created by Alexander P. Babich on 11/7/13.
//  Copyright (c) 2013 Alexander P. Babich. All rights reserved.
//

#ifndef GRASS_H
#define GRASS_H

#include <iostream>
#include "pokemon.h"

class Grass: public Pokemon
{
public:
    
    Grass();
    
    Grass(string pokemon_name, int pokemon_level, int pokemon_strength, int attack_type , int attack_level , int attack_power_points, int pokemon_chloraphil);
    
    void set_chloraphil(int chlor);
    
    int get_chloraphil() const;
    
    virtual void read();
    
    virtual void display();
    
private:
    
    int chloraphil;
    
};

#endif
