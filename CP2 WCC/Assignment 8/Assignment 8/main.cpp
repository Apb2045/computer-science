/*****************************************************************************
 Name: Alex Babich
 Date: Thursday, October 3rd 2013
 COMSC 110 Computer Programming II
 Assignment: Employee Vectors
 File Name: main.cpp
 Compiler Used: Xcode
 ****************************************************************************
 
 Problem Statement **********************************************************
 Put the problem statement in this section
 
 Use the Employee class from the text book to do the following. DO NOT modify the employee class header and implementation files.
 1. Create a program which creates an empty vector of Employee objects. Do not specify the size of the vector.
 2. The program should ask the user how many employees they want to put in the vector.
 3. Read the data for each employee the user wants to enter and add it to the vector.
 4. Display all the data in the vector.
 5. Create an additional Employee object and insert it into the middle of the
    vector. See the insert example in the textbook and adapt it.
 6. Remove one Employee object from the end of vector.
 7. Remove one Employee object from anywhere in the vector but not at the end.
 8. Display all the data in the final version of the vector.
 
 Data Requirements **********************************************************
 var name         data type      description
 
 Input:
 
 size             int           user inputs the size of the vector
 nameVector       string        user inputs the name of an employee into a vector
 salaryVector     double        user inputs the salary of an employee into a vector
 pos              int           user inputs the position that she would like to put a new employee object in / remove from vector
 salary           double        user inputs the salary of an employee
 name             string        user inputs the name of an employee
 
 Output:
 
 employeeVector   Employee       displays employees name and salary
 i                int            diplays the number of the employee
 
 
 Processing:
 
 employeeObject   Employee       used to store the user inputed name and salary and put it into the employeeVector
 buffer           string         holds the \n that is stored in memory so that the proper data can be accessed
 last             int            gets the value of the size of the employeeVector
 i                int            used for iteration through for loops
 pos              int            used to store the position that the user inputs and move the postions of the employeeVector
 
 
 Formulas: none
 
 
 Libraries:       iostream    string    vector    ccc_empl.h
 
 Algorithm Design ***********************************************************
 
 1. Asks the user how many employees he or she would like to input
 
 2. Has the user input the employees names into a name vector and the employees salaries in a salary vector
 
 3. Displays the names and salaries of the employees in one vector
 
 4. Asks the user in which position they would like to input a new employee
 
 5. Asks the user to enter the new employees name and salary
 
 6. Puts the new employee in the employee vector and then moves all of the ones behind it back one space
 
 7. Deletes the last entry of the employee vector
 
 8. Asks the user which entry besides the last one they would like to get rid of
 
 9. Displays the new array without the entry that was chosen by the user
 
 
 Testing Specifications *****************************************************
 Put test cases in this section
 
 1. made sure that the employee data fit in the corresponding vector indicies.
 
 2. verifyed that the new employee object displaces the proper index and the vectors size increses by 1.
 
 3. checked that the getline object was used properly.
 
 4. tested that the correct element was removed from the last index.
 
 4. identifyed the proper element was removed which came from a prompt by the user.
 
 
 */
// End of documentation

// Compiler directives : libraries and namespaces **************************

#include <iostream>
#include <vector>
#include "ccc_empl.h"
#include <string>

int main(int argc, const char * argv[])
{
    int size, //variable that stores the size of the vector
        pos; // used to pick a positing in the vector
    
    double salary; //stores the salary inputted by the user
    
    Employee employeeObject; // stores the salary and name of the employee given by the user
    
    string buffer, // used to clear out the '\n'
           name; //stores the name of the user
    
    
    cout << "Enter how many employees would you like to input: ";
    cin >> size;            //user chooses the size of the array
    cout << endl << endl;
    
    vector <Employee> employeeVector (size); //vector that holds all of the employee objects
    vector <string> nameVector (size); //vector that holds all of the names of employees
    vector <double> salaryVector (size); //vector that holds all of the salaries of employees
    
    for (int i = 0; i < size; i++) //loop that goes through each element of the array and has the user input the names and salaries of employees
    {
        cout << "Enter name of employee: ";
        getline(cin,buffer);
        getline(cin,nameVector[i]);
        cout << endl << endl
             << "Enter salary of employee: ";
        cin >> salaryVector[i];
        cout << endl << endl;
        
        employeeVector[i] = Employee(nameVector[i], salaryVector[i]); //all of the names and salaries are stored in the employee vector
        
    }
    
   
    for (int i = 0; i < size ; i++) // displays the current array
    {
        cout << "Employee " << i+1 << " name: "<< employeeVector[i].get_name() << "         "
             << "Employee " << i+1 << " salary: $"
             << employeeVector[i].get_salary() << endl << endl;
    }
   
    
    do
    {
    cout << "What position would you like to place a new employee into: ";
    cin >> pos; //gets the position from the user that the new ojecte will displaced
    cout << endl << endl;
    }
    
    
    while( 0 > pos || pos > size); //makes sure that the place that the user wants put a new object is alvailable
    
    pos = pos - 1; // allows the user to put the number that is intuitive for them
    
   
    //gets the name and the salary of the new employee
    cout << "Enter name of employee: ";
    getline(cin,buffer);
    getline(cin,name);
    cout << endl << endl
    << "Enter salary of employee: ";
    cin >> salary;
    cout << endl << endl;
    
    employeeObject = Employee(name, salary); //puts the name and salary in an employee object
    
    
    
    int last = employeeVector.size() - 1; //creats a variable that is the last index of the array
    
    employeeVector.push_back(employeeVector[last]); //puts the last element of the vector into the new position
    
    for (int i = last; i > pos; i--) //moves every element in the vector to the next element
    {
        employeeVector[i] = employeeVector[i - 1];
    }
    
        employeeVector[pos] = employeeObject; // puts the newly created employee into the postion given

    
     employeeVector.pop_back(); //removes the last element from the vector
    
    do
    {
        cout << "Please choose a postion to remove from besides the last one: ";
        cin >> pos;         //the user chooses the element which will be removed
        cout << endl << endl;
    }
    
    
    while( 0 > pos || pos > size - 1); //makes sure that the user chooses an available index
    
    pos = pos - 1; //allows the user the choose the element intuitively
    
    for (int i = pos; i < employeeVector.size() - 1; i++)
    {
        employeeVector[i] = employeeVector[i + 1]; // moves elements back one starting at the one chosen by the user
    }
    employeeVector.pop_back(); // deletes the last element of the vector
    
    
    cout << "Now here is the final version of your array" << endl << endl;
    
    for (int i = 0; i < size - 1 ; i++) // displays final verson of the array
    {
        cout << "Employee " << i+1 << " name: "<< employeeVector[i].get_name() << "         "
        << "Employee " << i+1 << " salary: $"
        << employeeVector[i].get_salary() << endl << endl;
    }
    
     
    return 0;
}



