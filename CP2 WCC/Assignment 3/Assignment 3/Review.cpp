//  ****************************************************************************
//  Name: Alex Babich
//  Date: 9/13/13
//  COMSC 110 Computer Programming II
//  Assignment: Review Program Multiple files
//  File Name: Review.cpp
//  Compiler Used: Xcode
//  ****************************************************************************

//  Compiler directives ********************************************************
#include <iostream>
#include "Review.h"
using namespace std;



//  Class member function implementations **************************************
//
void minMax (int & min, int & max)
{
	do //asks the user to enter a value between 1 and 500
	{
		cout << "enter a number for MINIMUM value between 1 and 500: ";
		cin >> min;
		cout << '\n' << '\n';
	}
	while(min < 1 || min > 500); //if the number entered is outside of the range it will prompt the user again
	
	do //asks the user to enter a value between the minimum value that they had just entered and 500
	{
		cout << "enter a number for MAXIMUM between your minimum value and 500: ";
		cin >> max;
		cout << '\n' << '\n';
	}
	while (max < min || max > 500); //if the number entered is outside of the range it will prompt the user again
}

int randNum (int low, int high) 
{
	return ( low + rand( ) % (high - low + 1 ) ); //creates random numbers between the user defined boundaries using the clock
}

void calcTotals (const int anyArray[], int & oddTotal, int & evenTotal, int SIZE)
{
	for (int x = 0; x < SIZE; x++) //goes through the array from the first value to the last
	{
		if (anyArray[x] % 2 == 0) //checks if the entry is even
		{
			evenTotal = evenTotal + anyArray[x]; //if the entry is even it then adds to the total of all of the even numbers
		}
		
		else
		{
			oddTotal = oddTotal + anyArray[x]; //if the number is not even it adds it to the odd total
		}
		
	}
	
}

void displayArray (int anArray[], int theOddTotal, int theEvenTotal)
{
	float sqrtEven = sqrt(theEvenTotal), //gets the square root of the even and the odd totals
    sqrtOdd = sqrt(theOddTotal);
	
	cout << "The array is: [ ";
	
	for (int j = 0; j < 25; j++)
    {
        
        cout << anArray[j] << " "; //displays the array
    }
    
	cout << "]" << '\n'<< '\n';
	
	cout << "The total of all of the even numbers is: " << theEvenTotal << '\n' << '\n'; //displays the even total
	
    cout << "The total of all of the odd numbers is: " << theOddTotal << '\n'<< '\n'; //displays the odd total
    
	cout << "The square root of the total of the even numbers is: " << sqrtEven << '\n'<< '\n'; //displays the square root of the even total
	
	cout << "The square root of the total of the odd numbers is: "<<  sqrtOdd << '\n'<< '\n'; //displays the  square root of the odd total
    
}


// Non-member function implementations *****************************************