//  ****************************************************************************
//  Name: Alex Babich
//  Date: 9/13/13
//  COMSC 110 Computer Programming II
//  Assignment: Review Program Multiple files
//  File Name: Review.h
//  Compiler Used: Xcode
//  ****************************************************************************

//  Compiler directives
#ifndef Review_H //checks to see if review.h has already been defined
#define Review_H //if it has not been defined then it will be defined

#include <iostream>
#include <cmath>

using namespace std;

//Constructs Review()


// Reads in this ArrayMath object

void minMax (int & min, int & max); // gets the lower and upper limits for a random number

int randNum (int low, int high); // generates a random number

void calcTotals (const int anyArray[], int & oddTotal, int & evenTotal, int SIZE); //gives you the odd total and the even total of the elements of an array

void displayArray (int anArray[], int theOddTotal, int theEvenTotal); //displays the array and also calculates the square root of the odd and even totals


#endif //ends header file





