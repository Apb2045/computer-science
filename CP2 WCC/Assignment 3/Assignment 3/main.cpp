/*
 ****************************************************************************
 Name: Alex Babich
 Date: 9/12/13
 COMSC 110 Computer Programming II
 Assignment: Review Program Multiple files
 File Name: main.cpp
 Compiler Used: Xcode
 ****************************************************************************
 
 Problem Statement **********************************************************
 Put the problem statement in this section:
 
 Write a program to randomly generate integer data values into an array which can hold 25 values. The
 program should ask the user to enter the minimum and maximum values in a numeric range that is not
 less than 1 and not greater than 500. Validate that the user enters the numbers correctly (minimum first >=
 1 and then maximum <= 500) and correct the errors if not. The program should then generate 25 random
 numbers in that range and store them in the array. After filling the array, the program should calculate the
 total of all the values that are even numbers and the total of all the values that are odd numbers. Then the
 program should calculate the square root of each of those totals (hint: cmath library function). Display all
 25 values in the array. Display the total of the even numbers and the square root of that total. Display the
 total of the odd numbers and the square root of that total.
 
 
 Data Requirements **********************************************************
 Input Variables: min // minimum value of the random numbers
 max // maximum value of the random numbers
 
 Output Variables: anArray[] // the array that is outputted
 theOddTotal // the total of all of the odd values of the array
 theEvenTotal // the total of all of the even values of the array
 sqrtOdd // the square root of the total of all of the odd values from the array
 sqrtEven // the square root of the total of all of the even values from the array
 
 Processing Variables: SIZE // used to give the size to the array.
 low // accepts the min value and is used to give the lower boundary for random number generation
 high // accepts the max value and is used to give the upper boundary for random number generation
 oddTotal // the total of the odds which is passed to theOddTotal
 evenTotal // the total of the evens which is passed to theEvenTotal
 myArray // the origional array that is passed to functions
 anyArray // an array that is used in the calc totals function. myArray is passed to it.
 i, j, x // interger used for iteration through loops
 
 Relevant Formulas: ( low + rand( ) % (high - low + 1 ) ); // returns random numbers between high and low
 
 for (int x = 0; x < SIZE; x++)
 {
 if (anyArray[x] % 2 == 0)
 {
 evenTotal = evenTotal + anyArray[x]; // calculates the even and odd totals
 }
 
 else
 {
 oddTotal = oddTotal + anyArray[x];
 }
 
 }
 
 sqrtEven = sqrt(theEvenTotal) // calculates the square root of the total of the even numbers
 
 
 Algorithm Design ***********************************************************
 Put the algorithm solution in this section:
 
 -Get the lower and upper number from the user to form limits for the random numbers
 -Have a function use those limits to return random numbers
 -Use a different function that puts the random numbers generated from the previous function into elements of an array
 -Have another functon that sorts through the array and adds up all of the odd and even numbers in to 2 separate groups
 -Use some variables to calculate the square root of the odd and even totals
 -Finally output the array and all of the totals that we have calculated
 
 Testing Specifications *****************************************************
 Put test cases in this section:
 

 
 */
// End of documentation

// Compiler directives : libraries and namespaces **************************

#include <iostream>
#include <string>
#include <cmath>
#include <ctime>
#include "Review.h"

using namespace std;

// Global Variables*********************************************************

const int SIZE = 25;


// main function ***********************************************************

int main ()
{
	srand(time( 0 ) );
	
	int min, max, //minimum and maximum values for numbers in array
	oddTotal = 0, //total of the odd numbers in the array
	evenTotal = 0, //total of the even numbers in the array
	myArray[SIZE]; //the array that we will be filling out
	
	minMax (min, max); // funciton that gets the minimum and maximum numbers from the user
	
	
	
	for (int i = 0; i < SIZE; i++) //fills the array with random numbers
	{
		myArray[i]= randNum (min, max);
	}
	
	calcTotals (myArray, oddTotal, evenTotal, SIZE); //calculates the totals of the even and odd numbers
	
	displayArray (myArray, oddTotal, evenTotal); //displays the arrays as well as the even and odd totals, also calculates the square root and displays that as well.
	
    
	// End of program statements
	cout << "Please press enter once or twice to continue...";
	cin.ignore().get();    			// hold console window open
	return EXIT_SUCCESS;           	// successful termination
    
}
// end main() ***************************************************************