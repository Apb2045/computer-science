#include <cstdlib>
#include <iostream>
#include "ccc_empl.h"
#include "ccc_time.h"
#include <string>


using namespace std;

int main()
{
    int hours = 0;
    
    int minutes = 0;
    
    int seconds = 0;
    
    int size;
    
    Time ** timeArray; //creates an array of pointers to time objects
    
    cout << "enter the number of time objects: ";
    cin >> size;
    cout << endl;
   
   
    
    for (int i=0; i < size; i++) //has the user enter the data which fills the array with pointers to the variables being inputted 
    {
        cout << "enter hours: ";
        
        cin >> hours;
        
        cout << "enter minutes: ";
        
        cin >> minutes;
        
        cout << "enter seconds: ";
        
        cin >> seconds;
        
        
        timeArray[i]= new Time(hours, minutes, seconds); 
        
    }
    
    for (int i = 0; i < size; i++) //displays the time of all of the objects by  dereferencing the pointer to get the value of the parameters 
    {
        cout << "hours: " << (*timeArray[i]).get_hours() << endl;
        
        cout << "minutes: " << (*timeArray[i]).get_minutes() << endl;
        
        cout << "seconds: " << (*timeArray[i]).get_seconds() << endl;
        
    }
    
    
    
    system("PAUSE");
    return EXIT_SUCCESS;
}
