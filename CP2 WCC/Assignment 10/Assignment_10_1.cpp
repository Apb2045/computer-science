#include <cstdlib>
#include <iostream>
#include "ccc_empl.h"
#include "ccc_time.h"
#include <vector>
#include <string>


using namespace std;

int main()
{
    int size = 0;
    
    string name = " ";
    
    double salary = 0.0;
    
    cout << "enter the number of employees: ";
    cin >> size;
    cout << endl;
   
    vector <Employee *> emps(size); //creates a vector of pointers to employee objects
    
    for (int i=0; i < size; i++) //has the user input the name and salary
    {
        cout << "enter employee name: ";
        
        cin >> name;
        
        cout << "enter employee salary: ";
        
        cin >> salary;
        
        emps[i]= new Employee(name, salary); //the memory address of these variables is stored in the vector
        
    }
    
    for (int i = 0; i < size; i++)
    {
        cout << "employee name: " << (*emps[i]).get_name() << endl; //dereferences the employee parameters so that they can be displayed
        
        cout << "employee salary: " << (*emps[i]).get_salary() << endl;
        
    }
    
    
    
    system("PAUSE");
    return EXIT_SUCCESS;
}
