//
//  pokemon.h
//  Assignment 9
//
//  Created by Alexander P. Babich on 10/5/13.
//  Copyright (c) 2013 Alexander P. Babich. All rights reserved.
//

#ifndef POKEMON_H
#define POKEMON_H

#include <iostream>
#include "attack.h"
#include <string.h>

using namespace std;

class Pokemon //creates a class pokemon
{
    
public:

    Pokemon(); //default constructor that holds default values for all standard objects as well as the default constructor for the attack object
    
    
    Pokemon(Attack pokemon_attack, string pokemon_name, int pokemon_type, int pokemon_level, int strength, int attack_type , int attack_level , int attack_power_points); //non default constructor requires
                                                                                                            //the user to input all values
                                                                                                            
    ~Pokemon();                                                                                                       
    
    
    void read();
    
    
    void display();
    
    
    void set_attack(int attack_type , int attack_level , int attack_power_points); //sets the attack to a new attack with the user inputed
                                                                                   //parameters
    
    void set_name(string new_name); //sets user inputed name of the pokemon
    
    
    void set_type(int new_type);//(1-fire, 2-water, 3-grass), sets the user inputed type of the pokemon
    
    
    void set_level(int new_level); //sets the user inputed level of the pokemon
    
    
    void set_strength(int new_strength); //sets the user inputed strength of the pokemon
    
    
    void set_status(bool new_status);
    
    
    Attack get_attack() const; //gets the attack obeject of the pokemon, attack member functions can be used to access the data
    
    
    string get_name() const; //gets the name of the pokemon to display
    
    
    int get_type() const; //gets the type of the pokemon to display
    
    
    int get_level() const; //gest the level of the pokemon to display
    
    
    int get_strength() const; // gets the strength of the pomemon to display
    
    
    bool get_status() const;
    
    
    
    
private: //private data members to be used by member functions of the pokemon class only
    
    Attack * attack;
    
    string name;
    
    int type;
    
    int level;
    
    int strength;
    
    bool status;
    
    int attackType;
    
    int attackLevel;
    
    int attackPowerPoints;
    
};

#endif
