#include <iostream>
#include <fstream>

using namespace std;

int main () 
{
	 	ofstream outData;
		outData.open("myFile.txt");
		
		string name;
		int salary;

		for(int i = 0; i < 3; i++)
		{
			cout << "Enter employee name: ";
			cin >> name;
			cout << "Enter employee salary: ";
			cin >> salary;
			
			outData << name << " "<< salary << endl;
		}
		
		outData.close();
		
		ifstream inData;
		
		inData.open("myFile.txt");
		
		char ch;
		while(inData.get(ch))
		{
			cout << ch << endl;
		}
}