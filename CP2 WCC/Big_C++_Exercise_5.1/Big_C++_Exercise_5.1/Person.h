#ifndef PERSON_H
#define PERSON_H
#include <string>

using namespace std;

class Person
{
    public:
    
    Person();
    Person(string pname, int page);
    void set_name(string pname);
    void set_age(int page);
    string get_name() const;
    int get_age() const;
    
    private:
    
    string name;
    int age;
    

};
#endif




