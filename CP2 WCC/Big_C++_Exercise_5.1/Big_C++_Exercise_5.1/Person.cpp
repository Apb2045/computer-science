#include "Person.h"
#include <string>



Person::Person()
{
    name = "John Smith";
    age = 45;
    
}

Person::Person(string pname, int page)
{
    name = pname;
    age = page;
}

void Person::set_name(string pname)
{
    name = pname;
}

void Person::set_age(int page)
{
    age = page;
}

string Person::get_name() const
{
    return name;
}

int Person::get_age() const
{
    return age;
}
    
