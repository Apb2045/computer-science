//
//  Book.cpp
//  Library Book
//
//  Created by Alex Babich on 12/4/14.
//  Copyright (c) 2014 Alex Babich. All rights reserved.
//

#include "Book.h"

Book::Book()
{
    author = "author";
    title = "title";
    ISBN = "ISBN";
    year = 0000;
}


Book::Book(string a,string i,string t,int y)
{
    author = a;
    
    ISBN = i;
    
    title = t;
    
    year = y;
}

void Book::set_author(string new_author)
{
    author = new_author;
}

void Book::set_ISBN(string new_ISBN)
{
    ISBN = new_ISBN;
}

void Book::set_title(string new_title)
{
    title = new_title;
}

void Book::set_year(int new_year)
{
    year = new_year;
}

string Book::get_author() const
{
    return author;
}

string Book::get_ISBN() const
{
    return ISBN;
}

string Book::get_title() const
{
    return title;
}

int Book::get_year() const
{
    return year;
}




