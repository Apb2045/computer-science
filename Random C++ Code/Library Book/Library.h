//
//  Library.h
//  Library Book
//
//  Created by Alex Babich on 12/4/14.
//  Copyright (c) 2014 Alex Babich. All rights reserved.
//

#ifndef __Library_Book__Library__
#define __Library_Book__Library__

#include <stdio.h>
#include "Book.h"
#include <string>
#include <map>

using namespace std;

class Library
{
    
public:
    
    Library();
    
    void add_book(Book b);
    
    void delete_Book(Book b);
    
    bool borrow_Book(Book b);
    
    void return_Book(Book b);
    
private:
    
    map<Book,bool> shelfer;
    
    
};

#endif /* defined(__Library_Book__Library__) */


