//
//  Library.cpp
//  Library Book
//
//  Created by Alex Babich on 12/4/14.
//  Copyright (c) 2014 Alex Babich. All rights reserved.
//

#include "Library.h"
#include <string>
#include <map>

using namespace std;



void Library::add_book(Book b)
{
    shelfer.insert( pair<Book, bool>(b, true) );
}

void Library::delete_Book(Book b)
{
    shelfer.erase(b);
}

bool Library::borrow_Book(Book b)
{
    
    if (shelfer[b] == true)
    {
        shelfer[b] = false;
        return true;
        
    }
    
    else return false;
}

void Library::return_Book(Book b)
{
    shelfer[b] = true;
}
