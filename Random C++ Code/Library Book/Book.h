//
//  Book.h
//  Library Book
//
//  Created by Alex Babich on 12/4/14.
//  Copyright (c) 2014 Alex Babich. All rights reserved.
//

#ifndef __Library_Book__Book__
#define __Library_Book__Book__

#include <string>

using namespace std;

class Book
{
    public:
    
        Book();
    
        Book(string a,string i,string t,int y);
   
        void set_author(string new_author);
    
        void set_ISBN(string new_ISBN);
    
        void set_title(string new_title);
    
        void set_year(int new_year);
    
        string get_author() const;
    
        string get_ISBN() const;
    
        string get_title() const;
    
        int get_year() const;
    
    
    bool operator < (const Book& b) const
    {
        return ISBN < b.ISBN;
    }
    
    
    private:
    
        string author;
    
        string ISBN;
    
        string title;
    
        int year;

    
};



#endif /* defined(__Library_Book__Book__) */
