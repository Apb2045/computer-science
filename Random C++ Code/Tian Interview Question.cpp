/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

Program that uses 100 by 100 matrix to represent solutions to function
of x and y.

Part 1: integrates each secton of the function individually saving each
in a new matrix and also returning the sum of the total area.

Part 2: takes the partial derivatives and saves the results in a new
matrix.


 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */


#include <iostream>
#include <iomanip>
using namespace std; 
int main(){
	
//Part 1 Integral 

int A[100][100];

for (int i = 0; i < 100; i++)
{	
	for(int j = 0; j < 100; j++)
	{
		A[j][i] = rand()%238; //fills 100 by 100 matrix with random numbers
	}
}

int B[99][99];

for (int i = 0; i < 99; i++)
{
	for(int j = 0; j < 99; j++)
	{	
		//fills a new array with average of the 4 points around a box 
		//which gives the height, base is one by one.
		//example if F(0,0) = 3 , F(1,0) = 6, F(0,1) = 5, F(1,1) = 3
		// sum would be 17 dividing the interger by 4 would give 4
		// remainder 1 mod 3 equals 1 so volume of section is 5
		B[j][i] = (A[j][i] + A[j][i+1] + A[j+1][i] + A[j+1][i+1])/4 
				  + (A[j][i] + A[j][i+1] + A[j+1][i] + A[j+1][i+1])%3;
	}
}

int volume = 0;

for (int i = 0; i < 99; i++)
{
	for(int j = 0; j < 99; j++)
	{	
		//Adds up each individual volume to get the total area under the function.
		volume += (B[j][i]);
	}
}

cout << "The volume of the integral is: " << volume << endl;


//Part 2 Derivatives

int DX[99][99]; //devivatives along rows, X partials

for (int i = 0; i < 99; i++)
{
	for(int j = 0; j < 99; j++)
	{	
		//fills a new array with difference between consecutive values of x.
		//change in x values is one, y is the same along each row.
		DX[j][i] = A[j+1][i] - A[j][i];
	}
}

int DY[99][99]; //devivatives along rows, Y partials

for (int i = 0; i < 99; i++)
{
	for(int j = 0; j < 99; j++)
	{	
		//fills a new array with difference between consecutive values of y.
		//change in y values is one, x is the same along each column.
		DX[j][i] = A[i][j+1] - A[i][j];
	}
}

return 0;

}