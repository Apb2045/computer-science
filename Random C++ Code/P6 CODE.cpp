/*
 * CSc103 Project 5: Syntax highlighting, part two.
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours: 
 */

#include "fsm.h"
using namespace cppfsm;
#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <string>
using std::string;
#include <set>
using std::set;
#include <map>
using std::map;
#include <initializer_list> // for setting up maps without constructors.

// enumeration for our highlighting tags:
enum {
	hlstatement,  // used for "if,else,for,while" etc...
	hlcomment,    // for comments
	hlstrlit,     // for string literals
	hlpreproc,    // for preprocessor directives (e.g., #include)
	hltype,       // for datatypes and similar (e.g. int, char, double)
	hlnumeric,    // for numeric literals (e.g. 1234)
	hlescseq,     // for escape sequences
	hlerror,      // for parse errors, like a bad numeric or invalid escape
	hlident       // for other identifiers.  Probably won't use this.
};

// usually global variables are a bad thing, but for simplicity,
// we'll make an exception here.
// initialize our map with the keywords from our list:
map<string, short> hlmap = {
#include "keywords.txt"
};
// note: the above is not a very standard use of #include...

// map of highlighting spans:
map<int, string> hlspans = {
	{hlstatement, "<span class='statement'>"},
	{hlcomment, "<span class='comment'>"},
	{hlstrlit, "<span class='strlit'>"},
	{hlpreproc, "<span class='preproc'>"},
	{hltype, "<span class='type'>"},
	{hlnumeric, "<span class='numeric'>"},
	{hlescseq, "<span class='escseq'>"},
	{hlerror, "<span class='error'>"}
};
// note: initializing maps as above requires the -std=c++0x compiler flag,
// as well as #include<initializer_list>.  Very convenient though.
// to save some typing, store a variable for the end of these tags:
string spanend = "</span>";

string translateHTMLReserved(char c) {
	switch (c) {
		case '"':
			return "&quot;"; 
		case '\'':
			return "&apos;";
		case '&':
			return "&amp;";
		case '<':
			return "&lt;";
		case '>':
			return "&gt;";
		case '\t': // make tabs 4 spaces instead.
			return "&nbsp;&nbsp;&nbsp;&nbsp;";
		default:
			char s[2] = {c,0};
			return s;
	}
}

string line(string s)
{

	int state = 0;
	int oldstate = 0;
	string total;
	string current;
	string html;
	map<int,int> state2html = {
	{2,1},{3,2},{6,5},{5,6},{7,7}
	};

	
	for(size_t i = 0; i < s.length(); i++)
	{
		
		oldstate = updateState(state, s[i]);
		current = current + s[i];
		
		if(s[i] == '#')
		{
			total = hlspans[hlpreproc] + s[i];
			i++;
			while (updateState(state,s[i]) == updateState(state,s[i+1]))
			{
				total = total +s[i];
				i++;
			}
			total = total + spanend;
		}
		
		
		else
		{
			map<string, short>::iterator it;
			it = hlmap.find(current);
			if(it != hlmap.end())
			{
				total = total + hlspans[hlmap[current]] + current + spanend;
				current = "";
				html = "";
			}
		html = html + translateHTMLReserved(s[i]);
		
	
		if(state != oldstate)
		{
			map<string, short>::iterator it;
			it = hlmap.find(current);
			if(it != hlmap.end())
			{
				total = total + hlspans[hlmap[current]] + current + spanend;
				current = "";
				html = "";
			}
			else
			{
			total = total + hlspans[state2html[oldstate]] + html + spanend;
			current = "";
			html = "";
			}
		}
		}
	}
	
	return total;
}

int main() {
	
	string n = "#include <iostream>";
/*	
	string token = "";
	string nums = "";
	string current = "";
	string html = "";
	map<int,string> m = {
	{0,"0"},{1,"1"},{2,"2"},{3,"3"},{4,"4"},{5,"5"},{6,"6"},{7,"7"}
	};
	//map<int,int> state2html = {
	//{0,
	
	int state = 0;
		cout << n << endl;
	for(size_t i = 0; i <n.length(); i++)
	{
		updateState(state,n[i]);
		cout << state;
		//x = x + translateHTMLReserved(n[i]);
	}
	
*/
	//while(getline(cin,n)) line(n);

	cout << line(n) << endl;
	
	
//	cout << x << endl;
/*
	for(size_t i = 0; i < x.length(); i++)
	{
		updateState(state,x[i]);
		nums = nums + m[state];
		cout << state;
	}
	
	cout << endl << nums << endl;
	
	for(size_t i = 0; i < x.length()-1; i++)
	{
		current = current + x[i];
	}
*/
/*
	if (nums[i] != nums[i+1])
	{
		map<string, short>::iterator it;
		it = hlmap.find(current);
		if(it != hlmap.end())
		{
			html = hlspans[hlmap[current]] + current + spanend;
			current = "";
		}
		else
		{
			
		}
		
	}
*/
	//getline(cin,n);
	//cout << line(n);
	
	return 0;
}
